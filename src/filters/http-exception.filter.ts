import {
    ExceptionFilter,
    Catch,
    ArgumentsHost,
    HttpException,
} from '@nestjs/common'
import { Response } from 'express'

/* Zwracaj właściwość message odpowiedzi jako tablicę,
 * nawet jeśli jest tylko jedna (domyślnie wtedy był string) */
@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
    catch(exception: HttpException, host: ArgumentsHost) {
        const ctx = host.switchToHttp()
        const response = ctx.getResponse<Response>()
        // const request = ctx.getRequest<Request>()
        const status = exception.getStatus()

        const standardMessage = exception.getResponse()
        const output: object =
            typeof standardMessage === 'string'
                ? { message: [standardMessage] }
                : standardMessage

        if (typeof output['message'] === 'string') {
            output['message'] = [output['message']]
        }

        response.status(status).json(output)
    }
}
