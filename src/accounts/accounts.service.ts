import {
    Injectable,
    NotFoundException,
    Inject,
    forwardRef,
} from '@nestjs/common'
import { Account } from './account.entity'
import { CreateAccountDto } from './dto/create-account.dto'
import { UpdateAccountDto } from './dto/update-account.dto'
import { AccountRepository } from './account.repository'
import { InjectRepository } from '@nestjs/typeorm'
import { User } from '../auth/user.entity'
import { UtilitiesService } from '../utilities/utilities.service'

@Injectable()
export class AccountsService {
    constructor(
        @InjectRepository(AccountRepository)
        private accountRepository: AccountRepository,
        @Inject(forwardRef(() => UtilitiesService))
        private utilitiesService: UtilitiesService,
    ) {}

    async getAccounts(user: User): Promise<Account[]> {
        const accounts = await this.accountRepository.getAccounts(user)

        for (const account of accounts) {
            await this.bindAccountBalance(account, user)
        }

        return accounts
    }

    async getAccountById(id: number, user: User): Promise<Account> {
        const account = await this.accountRepository.getAccountById(id, user)

        if (!account) {
            throw new NotFoundException(`Konto o ID "${id}" nie istnieje`)
        }

        await this.bindAccountBalance(account, user)

        return account
    }

    async createAccount(
        createAccountDto: CreateAccountDto,
        user: User,
    ): Promise<Account> {
        const defaultAccount: Account = await this.accountRepository.getDefaultAccount(
            user,
        )

        if (createAccountDto.default && defaultAccount) {
            defaultAccount.default = false
            await defaultAccount.save()
        }

        const account = await this.accountRepository.createAccount(
            createAccountDto,
            user,
        )

        if (!createAccountDto.default && !defaultAccount) {
            const modified = await this.setFirstAccountAsDefault(user)

            // jeśli zmodyfikowano dodawane konto, zwróć w odpowiedzi aktualny stan
            if (modified && modified.id === account.id) {
                account.default = true
            }
        }

        const initialBalanace = createAccountDto.currentBalance || 0

        if (initialBalanace !== 0) {
            await this.utilitiesService.createAccountInitialTransaction(
                account.id,
                initialBalanace,
                user,
            )
        }

        // zawrzej w odpowiedzi
        account.currentBalance = initialBalanace

        return account
    }

    async updateAccount(
        id: number,
        updateAccountDto: UpdateAccountDto,
        user: User,
    ): Promise<Account> {
        const account = await this.getAccountById(id, user)
        const defaultAccount: Account = await this.accountRepository.getDefaultAccount(
            user,
        )

        UpdateAccountDto.allDtoProperties.forEach(prop => {
            if (updateAccountDto[prop] !== undefined) {
                account[prop] = updateAccountDto[prop]
            }
        })

        // możliwe, że odarchiwizujemy pierwsze konto, wtedy oznacz jako domyślne
        if (!defaultAccount) {
            account.default = true
        }

        // konto zarchiwizowane nie może być domyślne
        if (account.archived) {
            account.default = false
        }

        await account.save()

        if (defaultAccount) {
            if (account.default && account.id !== defaultAccount.id) {
                defaultAccount.default = false
                await defaultAccount.save()
            }

            // przy odznaczaniu wskaż inne konto
            if (!account.default && account.id === defaultAccount.id) {
                const modified = await this.setFirstAccountAsDefault(
                    user,
                    account,
                )

                // jeśli zmodyfikowano edytowane konto, zwróć w odpowiedzi aktualny stan
                if (modified && modified.id === account.id) {
                    account.default = true
                }
            }
        }

        await this.bindAccountBalance(account, user)

        return account
    }

    async deleteAccount(id: number, user: User): Promise<void> {
        const account = await this.getAccountById(id, user)
        const result = await this.accountRepository.deleteAccount(id, user)

        if (result.affected < 1) {
            throw new NotFoundException(`Konto o ID "${id}" nie istnieje`)
        }

        // do konta mogły być przypisane transakcje
        this.utilitiesService.recalculateTransactions(user)

        if (account.default) {
            await this.setFirstAccountAsDefault(user)
        }
    }

    // zwraca konto, które zmodyfikowano LUB null
    private async setFirstAccountAsDefault(
        user: User,
        tryIgnoreAccount?: Account,
    ): Promise<Account> {
        const accounts = await this.getAccounts(user)
        let account =
            accounts.find(
                account =>
                    !account.archived &&
                    (tryIgnoreAccount
                        ? account.id !== tryIgnoreAccount.id
                        : true),
            ) || null

        // przy braku innych opcji nie pomiń wskazanego konta
        if (!account && tryIgnoreAccount && !tryIgnoreAccount.archived) {
            account = tryIgnoreAccount
        }

        if (account) {
            account.default = true
            await account.save()
        }

        return account
    }

    private async bindAccountBalance(
        account: Account,
        user: User,
    ): Promise<void> {
        account.currentBalance = await this.utilitiesService.getAccountBalance(
            account.id,
            user,
        )
    }
}
