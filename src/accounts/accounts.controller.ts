import {
    Controller,
    Get,
    Post,
    Body,
    Param,
    ParseIntPipe,
    Delete,
    Put,
    UsePipes,
    ValidationPipe,
    UseGuards,
} from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { AccountsService } from './accounts.service'
import { Account } from './account.entity'
import { CreateAccountDto } from './dto/create-account.dto'
import { UpdateAccountDto } from './dto/update-account.dto'
import { GetUser } from '../auth/get-user.decorator'
import { User } from '../auth/user.entity'

@Controller('accounts')
@UseGuards(AuthGuard())
export class AccountsController {
    constructor(private accountsService: AccountsService) {}

    @Get()
    getAccounts(@GetUser() user: User): Promise<Account[]> {
        return this.accountsService.getAccounts(user)
    }

    @Post()
    @UsePipes(ValidationPipe)
    createAccount(
        @Body() createAccountDto: CreateAccountDto,
        @GetUser() user: User,
    ): Promise<Account> {
        return this.accountsService.createAccount(createAccountDto, user)
    }

    @Get('/:id')
    getAccountById(
        @Param('id', ParseIntPipe) id: number,
        @GetUser() user: User,
    ): Promise<Account> {
        return this.accountsService.getAccountById(id, user)
    }

    @Put('/:id')
    @UsePipes(ValidationPipe)
    updateAccount(
        @Param('id', ParseIntPipe) id: number,
        @Body() updateAccountDto: UpdateAccountDto,
        @GetUser() user: User,
    ): Promise<Account> {
        return this.accountsService.updateAccount(id, updateAccountDto, user)
    }

    @Delete('/:id')
    deleteAccount(
        @Param('id', ParseIntPipe) id: number,
        @GetUser() user: User,
    ): Promise<void> {
        return this.accountsService.deleteAccount(id, user)
    }
}
