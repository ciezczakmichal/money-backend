import { BaseAccountDto } from './base-account.dto'
import { IsBoolean, IsOptional } from 'class-validator'

export class UpdateAccountDto extends BaseAccountDto {
    /* Przeładowane właściwości z klasy bazowej */

    @IsOptional()
    name?: string

    /* Nowe właściwości */

    @IsOptional()
    @IsBoolean()
    archived?: boolean

    static allDtoProperties = [...BaseAccountDto.allDtoProperties, 'archived']
}
