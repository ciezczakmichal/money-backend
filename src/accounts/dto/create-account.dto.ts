import { BaseAccountDto } from './base-account.dto'
import { IsOptional, IsNumber } from 'class-validator'

export class CreateAccountDto extends BaseAccountDto {
    // wymagaj nazwy
    name: string

    // kwota zasilająca
    @IsOptional()
    @IsNumber(
        { allowInfinity: false, allowNaN: false, maxDecimalPlaces: 2 },
        {
            message:
                'Kwota powinna być liczbą z maksymalnie 2 miejscami po przecinku',
        },
    )
    currentBalance?: number
}
