import { IsNotEmpty, IsString, IsBoolean, IsOptional } from 'class-validator'

export class BaseAccountDto {
    @IsNotEmpty()
    @IsString()
    name?: string

    @IsOptional()
    @IsString()
    description?: string

    @IsOptional()
    @IsString()
    iconName?: string

    @IsOptional()
    @IsBoolean()
    default?: boolean

    static allDtoProperties = ['name', 'description', 'iconName', 'default']
}
