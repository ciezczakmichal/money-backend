import { Test } from '@nestjs/testing'
import { AccountsService } from './accounts.service'
import { AccountRepository } from './account.repository'
import { UtilitiesService } from '../utilities/utilities.service'
import { UpdateAccountDto } from './dto/update-account.dto'
import { CreateAccountDto } from './dto/create-account.dto'
import { User } from '../auth/user.entity'

const mockUser = null

const mockAccountFactory = ({ id, name, default_, archived }) => ({
    id,
    name,
    default: default_,
    archived,

    save: jest.fn().mockResolvedValue(true),
})

const mockAccountRepository = () => {
    let accounts = {}
    accounts[0] = mockAccountFactory({
        id: 0,
        name: 'A',
        default_: false,
        archived: false,
    })
    accounts[1] = mockAccountFactory({
        id: 1,
        name: 'B',
        default_: true,
        archived: false,
    })
    accounts[2] = mockAccountFactory({
        id: 2,
        name: 'C',
        default_: false,
        archived: false,
    })

    const getLength = () => Object.keys(accounts).length
    let nextId = getLength()

    return {
        _accounts: (index: number) =>
            index !== undefined ? accounts[index] : accounts,
        _clear: () => (accounts = []),

        getAccounts: jest.fn(async (_user: User) => Object.values(accounts)),
        getAccountById: jest.fn(async (id, _user: User) => accounts[id]),
        getDefaultAccount: jest.fn(async (_user: User) =>
            Object.values(accounts).find((value: any) => value.default),
        ),
        createAccount: jest.fn(async (dto: CreateAccountDto, _user: User) => {
            const newAccount = mockAccountFactory({
                id: nextId,
                name: dto.name,
                default_: dto.default,
                archived: false,
            })
            accounts[newAccount.id] = newAccount
            nextId++
            return newAccount
        }),
        deleteAccount: jest.fn(async (id: number, _user: User) => {
            const prevLength = getLength()
            delete accounts[id]

            // obiekt TypeORM
            return { affected: prevLength - getLength() }
        }),
    }
}

const mockUtilitiesService = () => {
    return {
        recalculateTransactions: jest.fn().mockResolvedValue(true),
        getAccountBalance: jest.fn().mockResolvedValue(1234.56),
        createAccountInitialTransaction: jest.fn().mockResolvedValue(undefined),
    }
}

describe('AccountsService', () => {
    let accountsService: AccountsService
    let accountRepository: any

    beforeEach(async () => {
        const module = await Test.createTestingModule({
            providers: [
                AccountsService,
                {
                    provide: AccountRepository,
                    useFactory: mockAccountRepository,
                },
                {
                    provide: UtilitiesService,
                    useFactory: mockUtilitiesService,
                },
            ],
        }).compile()

        accountsService = module.get<AccountsService>(AccountsService)
        accountRepository = module.get<AccountRepository>(AccountRepository)
    })

    it('test funkcji mock-a', async () => {
        for (let i = 0; i < accountRepository._accounts.length; i++) {
            expect(await accountRepository.getAccountById(i, mockUser)).toEqual(
                accountRepository._accounts(i),
            )
        }

        expect(await accountRepository.getDefaultAccount(mockUser)).toEqual(
            accountRepository._accounts(1),
        )

        // test edycji
        let account = await accountRepository.getAccountById(1, mockUser)
        expect(account.name).toEqual('B')
        account.name = 'test'

        account = await accountRepository.getAccountById(1, mockUser)
        expect(account.name).toEqual('test')
    })

    describe('ustawianie domyślnego konta', () => {
        it('zmiana innego konta na domyślne', async () => {
            const dto: UpdateAccountDto = { default: true }
            const result = await accountsService.updateAccount(2, dto, mockUser)

            let account = accountRepository._accounts(1)
            expect(account.default).toEqual(false)
            expect(account.save).toHaveBeenCalled()

            account = accountRepository._accounts(2)
            expect(account.default).toEqual(true)
            expect(account.save).toHaveBeenCalled()

            // konto 0 nie powinno zostać zmodyfikowane
            account = accountRepository._accounts(0)
            expect(account.default).toEqual(false)
            expect(account.save).not.toHaveBeenCalled()

            expect(result).toEqual(accountRepository._accounts(2))
            expect(result.default).toEqual(true)

            // @todo test dedykowanej funkcji - jeśli będzie w service
        })

        it('odznaczenie konta domyślnego', async () => {
            const dto: UpdateAccountDto = { default: false }
            const result = await accountsService.updateAccount(1, dto, mockUser)

            let account = accountRepository._accounts(1)
            expect(account.default).toEqual(false)
            expect(account.save).toHaveBeenCalled()

            account = accountRepository._accounts(0)
            expect(account.default).toEqual(true)
            expect(account.save).toHaveBeenCalled()

            // konto 2 nie powinno zostać zmodyfikowane
            account = accountRepository._accounts(2)
            expect(account.default).toEqual(false)
            expect(account.save).not.toHaveBeenCalled()

            expect(result).toEqual(accountRepository._accounts(1))
            expect(result.default).toEqual(false)
        })

        it('odznaczenie konta domyślnego, które jest jedynym kontem', async () => {
            await accountsService.deleteAccount(0, mockUser)
            await accountsService.deleteAccount(1, mockUser)

            const dto: UpdateAccountDto = { default: false }
            const result = await accountsService.updateAccount(2, dto, mockUser)

            // konto 2 nie powinno zostać zmodyfikowane
            const account = accountRepository._accounts(2)
            expect(account.default).toEqual(true)
            expect(account.save).toHaveBeenCalled()

            expect(result).toEqual(accountRepository._accounts(2))
            expect(result.default).toEqual(true) // tutaj główna różnica
        })

        it('odznaczenie konta domyślnego, które jest pierwszym kontem na liście', async () => {
            let dto: UpdateAccountDto = { default: true }
            await accountsService.updateAccount(0, dto, mockUser)

            expect(accountRepository._accounts(0).default).toEqual(true)

            // test

            dto = { default: false }
            const result = await accountsService.updateAccount(0, dto, mockUser)

            let account = accountRepository._accounts(0)
            expect(account.default).toEqual(false)
            expect(account.save).toHaveBeenCalled()

            account = accountRepository._accounts(1)
            expect(account.default).toEqual(true)
            expect(account.save).toHaveBeenCalled()

            expect(result).toEqual(accountRepository._accounts(0))
            expect(result.default).toEqual(false)
        })

        it('dodanie nowego konta oznaczonego jako domyślne', async () => {
            const dto: CreateAccountDto = { name: 'Nowe konto', default: true }
            const result = await accountsService.createAccount(dto, mockUser)

            let account = accountRepository._accounts(1)
            expect(account.default).toEqual(false)
            expect(account.save).toHaveBeenCalled()

            account = accountRepository._accounts(result.id)
            expect(account.default).toEqual(true)
            expect(account.save).not.toHaveBeenCalled()

            expect(result.default).toEqual(true)

            // @todo test dedykowanej funkcji - jeśli będzie w service
        })

        it('dodanie nowego konta, które nie jest domyślne', async () => {
            const dto: CreateAccountDto = { name: 'Nowe konto', default: false }
            const result = await accountsService.createAccount(dto, mockUser)

            let account = accountRepository._accounts(1)
            expect(account.default).toEqual(true)
            expect(account.save).not.toHaveBeenCalled()

            account = accountRepository._accounts(result.id)
            expect(account.default).toEqual(false)
            expect(account.save).not.toHaveBeenCalled()

            expect(result.default).toEqual(false)

            // @todo test dedykowanej funkcji - jeśli będzie w service
        })

        it('dodanie pierwszego konta bez oznaczenia jako domyślne', async () => {
            accountRepository._clear()
            expect(accountRepository._accounts()).toEqual([])

            const dto: CreateAccountDto = {
                name: 'Pierwsze konto',
                default: false,
            }
            const result = await accountsService.createAccount(dto, mockUser)

            const account = accountRepository._accounts(result.id)
            expect(account.default).toEqual(true)
            expect(account.save).toHaveBeenCalled()

            expect(result.default).toEqual(true)

            // @todo test dedykowanej funkcji - jeśli będzie w service
        })

        it('usunięcie konta, które nie jest domyślne', async () => {
            await accountsService.deleteAccount(0, mockUser)

            expect(accountRepository._accounts(0)).toEqual(undefined)

            const account = accountRepository._accounts(1)
            expect(account.default).toEqual(true)
            expect(account.save).not.toHaveBeenCalled()
        })

        it('usunięcie konta, które jest obecnie domyślne', async () => {
            await accountsService.deleteAccount(1, mockUser)

            expect(accountRepository._accounts(1)).toEqual(undefined)

            let account = accountRepository._accounts(0)
            expect(account.default).toEqual(true)
            expect(account.save).toHaveBeenCalled()

            account = accountRepository._accounts(2)
            expect(account.default).toEqual(false)
            expect(account.save).not.toHaveBeenCalled()
        })

        it('dwukrotne usunięcie domyślnych kont', async () => {
            await accountsService.deleteAccount(1, mockUser)
            await accountsService.deleteAccount(0, mockUser)

            const account = accountRepository._accounts(2)
            expect(account.default).toEqual(true)
            expect(account.save).toHaveBeenCalled()
        })
    })

    describe('zmiana ustawienia archiwizacji a ustawianie domyślnego konta', () => {
        it('zarchiwizowanie domyślnego konta, gdy kont więcej', async () => {
            expect(accountRepository._accounts(1).archived).toEqual(false)
            expect(accountRepository._accounts(1).default).toEqual(true)

            const dto: UpdateAccountDto = { archived: true }
            const result = await accountsService.updateAccount(1, dto, mockUser)

            let account = accountRepository._accounts(1)
            expect(account.default).toEqual(false)
            expect(account.archived).toEqual(true)
            expect(account.save).toHaveBeenCalled()

            account = accountRepository._accounts(0)
            expect(account.default).toEqual(true)
            expect(account.archived).toEqual(false)
            expect(account.save).toHaveBeenCalled()

            expect(result).toEqual(accountRepository._accounts(1))
            expect(result.default).toEqual(false)
            expect(result.archived).toEqual(true)
        })

        it('zarchiwizowanie domyślnego konta, gdy jest to jedyne konto', async () => {
            await accountsService.deleteAccount(0, mockUser)
            await accountsService.deleteAccount(1, mockUser)
            expect(accountRepository._accounts(2).default).toEqual(true)

            const dto: UpdateAccountDto = { archived: true }
            const result = await accountsService.updateAccount(2, dto, mockUser)

            const account = accountRepository._accounts(2)
            expect(account.default).toEqual(false)
            expect(account.archived).toEqual(true)
            expect(account.save).toHaveBeenCalled()

            expect(result).toEqual(accountRepository._accounts(2))
            expect(result.default).toEqual(false)
            expect(result.archived).toEqual(true)
        })

        it('odarchiwizowanie jedynego konta', async () => {
            // przygotowania

            await accountsService.deleteAccount(0, mockUser)
            await accountsService.deleteAccount(1, mockUser)

            let dto: UpdateAccountDto = { archived: true }
            let result = await accountsService.updateAccount(2, dto, mockUser)

            expect(accountRepository._accounts(2).default).toEqual(false)

            // test

            dto = { archived: false }
            result = await accountsService.updateAccount(2, dto, mockUser)

            const account = accountRepository._accounts(2)
            expect(account.default).toEqual(true)
            expect(account.archived).toEqual(false)
            expect(account.save).toHaveBeenCalled()

            expect(result).toEqual(accountRepository._accounts(2))
            expect(result.default).toEqual(true)
            expect(result.archived).toEqual(false)
        })

        it('archiwizacja wszystkich kont, wybierając zawsze domyślne', async () => {
            const archiveAccountAndTest = async (id: number) => {
                const dto: UpdateAccountDto = { archived: true }
                const result = await accountsService.updateAccount(
                    id,
                    dto,
                    mockUser,
                )

                const account = accountRepository._accounts(id)
                expect(account.default).toEqual(false)
                expect(account.archived).toEqual(true)
                expect(account.save).toHaveBeenCalled()

                expect(result).toEqual(accountRepository._accounts(id))
                expect(result.default).toEqual(false)
                expect(result.archived).toEqual(true)
            }

            // sprawdź, czy wybrano właściwe domyślne konta
            expect(accountRepository._accounts(1).default).toEqual(true)
            await archiveAccountAndTest(1)
            expect(accountRepository._accounts(0).default).toEqual(true)
            await archiveAccountAndTest(0)
            expect(accountRepository._accounts(2).default).toEqual(true)
            await archiveAccountAndTest(2)
            expect(accountRepository._accounts(2).default).toEqual(false)
        })
    })
})
