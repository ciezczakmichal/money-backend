import { Repository, EntityRepository, DeleteResult } from 'typeorm'
import { Account } from './account.entity'
import { CreateAccountDto } from './dto/create-account.dto'
import { User } from '../auth/user.entity'

@EntityRepository(Account)
export class AccountRepository extends Repository<Account> {
    getAccounts(user: User): Promise<Account[]> {
        return this.find({
            where: { userId: user.id },
            order: { id: 'ASC' },
        })
    }

    getAccountById(id: number, user: User): Promise<Account> {
        return this.findOne({
            where: { id, userId: user.id },
        })
    }

    getDefaultAccount(user: User): Promise<Account> {
        return this.findOne({
            where: { default: true, userId: user.id },
        })
    }

    async createAccount(
        createAccountDto: CreateAccountDto,
        user: User,
    ): Promise<Account> {
        const { name, description, iconName } = createAccountDto

        const account = this.create()
        account.name = name
        account.description = description
        account.iconName = iconName
        account.default = createAccountDto.default
        account.archived = false
        account.user = user
        await account.save()

        delete account.user // nie zwracaj danych użytkownika
        return account
    }

    deleteAccount(id: number, user: User): Promise<DeleteResult> {
        return this.delete({ id, userId: user.id })
    }
}
