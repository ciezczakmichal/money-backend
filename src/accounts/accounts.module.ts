import { Module } from '@nestjs/common'
import { AccountsController } from './accounts.controller'
import { AccountsService } from './accounts.service'
import { TypeOrmModule } from '@nestjs/typeorm'
import { AccountRepository } from './account.repository'
import { AuthModule } from '../auth/auth.module'
import { UtilitiesModule } from '../utilities/utilities.module'

@Module({
    imports: [
        TypeOrmModule.forFeature([AccountRepository]),
        AuthModule,
        UtilitiesModule,
    ],
    controllers: [AccountsController],
    providers: [AccountsService],
    exports: [AccountsService],
})
export class AccountsModule {}
