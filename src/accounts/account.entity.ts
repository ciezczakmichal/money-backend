import {
    BaseEntity,
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToOne,
    OneToMany,
} from 'typeorm'
import { User } from '../auth/user.entity'
import {
    ExternalTransaction,
    AccountTransfer,
} from '../transactions/transaction.entity'

@Entity('accounts')
export class Account extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string

    @Column({ nullable: true })
    description: string

    @Column({ nullable: true })
    iconName: string

    @Column({ default: false })
    default: boolean

    @Column({ default: false })
    archived: boolean

    @ManyToOne(
        _type => User,
        user => user.accounts,
        { eager: false, onDelete: 'CASCADE' },
    )
    user: User

    @Column()
    userId: number

    // używane w odpowiedziach
    currentBalance: number

    /* Powiązania z transakcjami */

    @OneToMany(
        _type => ExternalTransaction,
        transaction => transaction.account,
        { eager: false },
    )
    externalTransactions: ExternalTransaction[]

    @OneToMany(
        _type => AccountTransfer,
        transaction => [
            transaction.sourceAccount,
            transaction.destinationAccount,
        ],
        { eager: false },
    )
    transferTransactions: AccountTransfer[]
}
