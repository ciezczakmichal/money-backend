import { Repository, EntityRepository } from 'typeorm'
import { User } from './user.entity'
import { AuthCredentialsDto } from './dto/auth-credentials.dto'
import { ConflictException, InternalServerErrorException } from '@nestjs/common'
import * as bcrypt from 'bcrypt'

@EntityRepository(User)
export class UserRepository extends Repository<User> {
    async signUp(authCredentialsDto: AuthCredentialsDto): Promise<User> {
        const { email, password } = authCredentialsDto

        const user = this.create()
        user.email = email
        user.salt = await bcrypt.genSalt()
        user.password = await this.hashPassword(password, user.salt)

        try {
            await user.save()
            return user
        } catch (error) {
            // duplikat adresu e-mail
            if (error.code === '23505') {
                throw new ConflictException(
                    'Użytkownik z takim adresem e-mail już istnieje',
                )
            } else {
                throw new InternalServerErrorException()
            }
        }
    }

    // nie ma sposobu, żeby powiedzieć, że zwraca czasem null :c
    async validateUserPassword(
        authCredentialsDto: AuthCredentialsDto,
    ): Promise<string> {
        const { email, password } = authCredentialsDto
        const user = await this.findOne({ email })
        let result = null

        if (user) {
            const hash = await this.hashPassword(password, user.salt)

            if (user.password === hash) {
                result = user.email
            }
        }

        return result
    }

    private hashPassword(password: string, salt: string): Promise<string> {
        return bcrypt.hash(password, salt)
    }
}
