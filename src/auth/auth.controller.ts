import {
    Controller,
    Post,
    Body,
    ValidationPipe,
    UseGuards,
    Delete,
    HttpCode,
    Get,
} from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { AuthCredentialsDto } from './dto/auth-credentials.dto'
import { AuthService } from './auth.service'
import { AccessToken } from './interfaces/access-token.interface'
import { GetUser } from './get-user.decorator'
import { User } from './user.entity'
import { UserDataResponse } from './interfaces/user-data-response.interface'

@Controller('auth')
export class AuthController {
    constructor(private authService: AuthService) {}

    @Post('/sign-up')
    signUp(
        @Body(ValidationPipe) authCredentialsDto: AuthCredentialsDto,
    ): Promise<AccessToken> {
        return this.authService.signUp(authCredentialsDto)
    }

    @Post('/sign-in')
    signIn(
        @Body(ValidationPipe) authCredentialsDto: AuthCredentialsDto,
    ): Promise<AccessToken> {
        return this.authService.signIn(authCredentialsDto)
    }

    @Get('/user-data')
    @UseGuards(AuthGuard())
    getUserData(@GetUser() user: User): UserDataResponse {
        return this.authService.getUserData(user)
    }

    @Delete('/delete-user')
    @UseGuards(AuthGuard())
    deleteUser(@GetUser() user: User): Promise<void> {
        return this.authService.deleteUser(user)
    }

    @Post('/ping')
    @UseGuards(AuthGuard())
    @HttpCode(200)
    ping(@GetUser() user: User): Promise<void> {
        return this.authService.ping(user)
    }
}
