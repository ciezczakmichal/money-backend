import {
    BaseEntity,
    Entity,
    PrimaryGeneratedColumn,
    Column,
    Unique,
    OneToMany,
} from 'typeorm'
import { Category } from '../categories/category.entity'
import { Account } from '../accounts/account.entity'

@Entity('users')
@Unique(['email'])
export class User extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    email: string

    @Column()
    password: string

    @Column()
    salt: string

    @OneToMany(
        _type => Account,
        account => account.user,
        { eager: false },
    )
    accounts: Account[]

    @OneToMany(
        _type => Category,
        category => category.user,
        { eager: false },
    )
    categories: Category[]
}
