import {
    Injectable,
    BadRequestException,
    InternalServerErrorException,
} from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { JwtService } from '@nestjs/jwt'
import { UserRepository } from './user.repository'
import { AuthCredentialsDto } from './dto/auth-credentials.dto'
import { JwtPayload } from './interfaces/jwt-payload.interface'
import { AccessToken } from './interfaces/access-token.interface'
import { User } from './user.entity'
import { UtilitiesService } from '../utilities/utilities.service'
import { UserDataResponse } from './interfaces/user-data-response.interface'

@Injectable()
export class AuthService {
    constructor(
        @InjectRepository(UserRepository)
        private userRepository: UserRepository,
        private jwtService: JwtService,
        private utilitiesService: UtilitiesService,
    ) {}

    async signUp(authCredentialsDto: AuthCredentialsDto): Promise<AccessToken> {
        const user = await this.userRepository.signUp(authCredentialsDto)

        // dodaj przykładowe dane dla nowego użytkownika
        await this.utilitiesService.createBaseEntriesForUser(user)

        // zaloguj nowego użytkownika
        return this.signIn(authCredentialsDto)
    }

    async signIn(authCredentialsDto: AuthCredentialsDto): Promise<AccessToken> {
        const email = await this.userRepository.validateUserPassword(
            authCredentialsDto,
        )

        if (!email) {
            throw new BadRequestException(
                'Nieprawidłowe dane uwierzytelniające',
            )
        }

        const payload: JwtPayload = { email }
        const accessToken = await this.jwtService.signAsync(payload)

        return { accessToken }
    }

    getUserData(user: User): UserDataResponse {
        const response: UserDataResponse = {
            email: user.email,
        }
        return response
    }

    async deleteUser(user: User): Promise<void> {
        const { email } = user
        const result = await this.userRepository.delete({ email })

        // zawsze powinniśmy usunąć, bo użytkownik przeszedł uwierzytelnianie
        if (result.affected !== 1) {
            throw new InternalServerErrorException()
        }
    }

    async ping(_user: User): Promise<void> {
        // obecnie nie ma nic do roboty
    }
}
