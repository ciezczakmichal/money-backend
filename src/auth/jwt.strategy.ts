import { UnauthorizedException, Injectable } from '@nestjs/common'
import { PassportStrategy } from '@nestjs/passport'
import { Strategy, ExtractJwt } from 'passport-jwt'
import { InjectRepository } from '@nestjs/typeorm'
import { ConfigService } from '@nestjs/config'
import { UserRepository } from './user.repository'
import { JwtPayload } from './interfaces/jwt-payload.interface'
import { User } from './user.entity'

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(
        @InjectRepository(UserRepository)
        private userRepository: UserRepository,
        private configService: ConfigService,
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: configService.get<string>('jwt.secret'),
        })
    }

    async validate(payload: JwtPayload): Promise<User> {
        const { email } = payload
        const user = await this.userRepository.findOne({ email })

        if (!user) {
            throw new UnauthorizedException()
        }

        return user
    }
}
