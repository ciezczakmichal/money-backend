import { IsString, MinLength, MaxLength, IsEmail } from 'class-validator'

export class AuthCredentialsDto {
    @IsString()
    @IsEmail({}, { message: 'Nieprawidłowy adres e-mail' })
    email: string

    @IsString()
    @MinLength(6, {
        message: 'Hasło powinno składać się z co najmniej 6 znaków',
    })
    @MaxLength(30, { message: 'Hasło nie może zawierać więcej niż 30 znaków' })
    password: string
}
