import {
    Controller,
    Get,
    Post,
    Body,
    UsePipes,
    ValidationPipe,
    Param,
    ParseIntPipe,
    Put,
    Delete,
    UseGuards,
} from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import {
    CategoriesService,
    IncludeInternalCategories,
} from './categories.service'
import { CreateCategoryDto } from './dto/create-category.dto'
import { UpdateCategoryDto } from './dto/update-category.dto'
import { Category } from './category.entity'
import { GetUser } from '../auth/get-user.decorator'
import { User } from '../auth/user.entity'
import { GetCategoryDto } from './dto/get-category.dto'

@Controller('categories')
@UseGuards(AuthGuard())
export class CategoriesController {
    constructor(private categoriesService: CategoriesService) {}

    @Get()
    async getCategories(@GetUser() user: User): Promise<Category[]> {
        const categories = await this.categoriesService.getCategories(
            IncludeInternalCategories.Exclude,
            user,
        )
        categories.forEach(category => this.deleteInternalProperties(category))
        return categories
    }

    @Post()
    @UsePipes(ValidationPipe)
    async createCategory(
        @Body() createCategoryDto: CreateCategoryDto,
        @GetUser() user: User,
    ): Promise<Category> {
        const category = await this.categoriesService.createCategory(
            createCategoryDto,
            user,
        )
        this.deleteInternalProperties(category)
        return category
    }

    @Get('/:id')
    async getCategoryById(
        @Param('id', ParseIntPipe) id: number,
        @GetUser() user: User,
    ): Promise<GetCategoryDto> {
        const category = await this.categoriesService.getCategoryByIdWithTransactions(
            id,
            IncludeInternalCategories.Exclude,
            user,
        )
        this.deleteInternalProperties(category)
        return category
    }

    @Put('/:id')
    @UsePipes(ValidationPipe)
    async updateCategory(
        @Param('id', ParseIntPipe) id: number,
        @Body() updateCategoryDto: UpdateCategoryDto,
        @GetUser() user: User,
    ): Promise<Category> {
        const category = await this.categoriesService.updateCategory(
            id,
            updateCategoryDto,
            IncludeInternalCategories.Exclude,
            user,
        )
        this.deleteInternalProperties(category)
        return category
    }

    @Delete('/:id')
    deleteCategory(
        @Param('id', ParseIntPipe) id: number,
        @GetUser() user: User,
    ): Promise<void> {
        return this.categoriesService.deleteCategory(id, user)
    }

    private deleteInternalProperties(category: {
        internal: any
        internalName: any
    }): void {
        delete category.internal
        delete category.internalName
    }
}
