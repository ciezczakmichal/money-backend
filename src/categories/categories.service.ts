import {
    Injectable,
    NotFoundException,
    Inject,
    forwardRef,
} from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { CreateCategoryDto } from './dto/create-category.dto'
import { UpdateCategoryDto } from './dto/update-category.dto'
import {
    CategoryRepository,
    IncludeInternalCategories,
} from './category.repository'
import { Category } from './category.entity'
import { User } from '../auth/user.entity'
import { UtilitiesService } from '../utilities/utilities.service'
import { GetCategoryDto } from './dto/get-category.dto'

export { IncludeInternalCategories } from './category.repository'

export enum IncludeAmountCalculation {
    Include,
    Exclude,
}

@Injectable()
export class CategoriesService {
    constructor(
        @InjectRepository(CategoryRepository)
        private categoryRepository: CategoryRepository,
        @Inject(forwardRef(() => UtilitiesService))
        private utilitiesService: UtilitiesService,
    ) {}

    async getCategories(
        option: IncludeInternalCategories,
        user: User,
        calculation: IncludeAmountCalculation = IncludeAmountCalculation.Include,
    ): Promise<Category[]> {
        const categories = await this.categoryRepository.getCategories(
            option,
            user,
        )

        if (calculation === IncludeAmountCalculation.Include) {
            for (const category of categories) {
                await this.bindTotalAmountForCurrentMonth(category, user)
            }
        }

        return categories
    }

    async getCategoryById(
        id: number,
        option: IncludeInternalCategories,
        user: User,
    ): Promise<Category> {
        const category = await this.categoryRepository.getCategoryById(
            id,
            option,
            user,
        )

        if (!category) {
            throw new NotFoundException(`Kategoria o ID "${id}" nie istnieje`)
        }

        await this.bindTotalAmountForCurrentMonth(category, user)

        return category
    }

    async getCategoryByIdWithTransactions(
        id: number,
        option: IncludeInternalCategories,
        user: User,
    ): Promise<GetCategoryDto> {
        const category = await this.getCategoryById(id, option, user)
        const daysData = await this.utilitiesService.getTransactionsForCategory(
            id,
            user,
        )

        return {
            ...category,
            transactions: daysData,
        }
    }

    async createCategory(
        createCategoryDto: CreateCategoryDto,
        user: User,
    ): Promise<Category> {
        const category = await this.categoryRepository.createCategory(
            createCategoryDto,
            user,
        )
        category.amountForCurrentMonth = 0.0 // zawrzej w odpowiedzi
        return category
    }

    async updateCategory(
        id: number,
        updateCategoryDto: UpdateCategoryDto,
        option: IncludeInternalCategories,
        user: User,
    ): Promise<Category> {
        const category = await this.getCategoryById(id, option, user)
        const prevType = category.type

        UpdateCategoryDto.allDtoProperties.forEach(prop => {
            if (updateCategoryDto[prop] !== undefined) {
                category[prop] = updateCategoryDto[prop]
            }
        })
        await category.save()

        // zmiana typu kategorii całkowicie zmienia wyliczenia
        if (category.type !== prevType) {
            this.utilitiesService.recalculateTransactions(user)
        }

        // zmiana typu kategorii nie zmienia sum dla miesiąca
        await this.bindTotalAmountForCurrentMonth(category, user)

        return category
    }

    async deleteCategory(id: number, user: User): Promise<void> {
        const result = await this.categoryRepository.deleteCategory(id, user)

        if (result.affected < 1) {
            throw new NotFoundException(`Kategoria o ID "${id}" nie istnieje`)
        }

        // mogły zostać usunięte transakcje
        this.utilitiesService.recalculateTransactions(user)
    }

    async markCategoryAsInternal(
        categoryId: number,
        internalName: string,
        user: User,
    ): Promise<Category> {
        const category = await this.getCategoryById(
            categoryId,
            IncludeInternalCategories.Include,
            user,
        )

        category.internal = true
        category.internalName = internalName
        await category.save()

        return category
    }

    private async bindTotalAmountForCurrentMonth(
        category: Category,
        user: User,
    ): Promise<void> {
        const amount = await this.utilitiesService.getCategoryTotalAmountForMonth(
            category.id,
            user,
        )
        category.amountForCurrentMonth = amount
    }
}
