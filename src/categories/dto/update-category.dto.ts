import { BaseCategoryDto } from './base-category.dto'
import { IsBoolean, IsOptional } from 'class-validator'
import { CategoryType } from '../category-type.enum'

export class UpdateCategoryDto extends BaseCategoryDto {
    /* Przeładowane właściwości z klasy bazowej */

    @IsOptional()
    name: string

    @IsOptional()
    type: CategoryType

    /* Nowe właściwości */

    @IsOptional()
    @IsBoolean()
    archived: boolean

    static allDtoProperties = [...BaseCategoryDto.allDtoProperties, 'archived']
}
