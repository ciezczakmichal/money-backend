import { CategoryType } from '../category-type.enum'
import {
    IsNotEmpty,
    IsString,
    IsHexColor,
    IsOptional,
    IsEnum,
} from 'class-validator'

export class BaseCategoryDto {
    @IsNotEmpty()
    @IsString()
    name: string

    @IsEnum(CategoryType, {
        message: 'Niezdefiniowany lub nieprawidłowy typ kategorii',
    })
    type: CategoryType

    @IsOptional()
    @IsString()
    iconName: string

    @IsOptional()
    @IsHexColor()
    color: string

    static allDtoProperties = ['name', 'type', 'iconName', 'color']
}
