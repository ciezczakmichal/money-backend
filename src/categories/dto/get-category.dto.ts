import { CategoryType } from '../category-type.enum'
import { TransactionsPerDay } from '../../transactions/dto/transaction-list.dto'

export interface GetCategoryDto {
    id: number
    name: string
    type: CategoryType
    iconName: string
    color: string
    archived: boolean
    userId: number
    internal: boolean
    internalName: string
    amountForCurrentMonth: number

    transactions: TransactionsPerDay[]
}
