/**
 * Rodzaj kategorii (wydatek lub przychód).
 */
export enum CategoryType {
    /** wydatek (np. zakupy) */
    Expense = 'EXPENSE',

    /** przychód (np. wynagrodzenie) */
    Proceeds = 'PROCEEDS',
}
