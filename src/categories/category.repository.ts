import { Repository, EntityRepository, DeleteResult } from 'typeorm'
import { Category } from './category.entity'
import { CreateCategoryDto } from './dto/create-category.dto'
import { User } from '../auth/user.entity'

export enum IncludeInternalCategories {
    Exclude,
    Include,
}

@EntityRepository(Category)
export class CategoryRepository extends Repository<Category> {
    getCategories(
        option: IncludeInternalCategories,
        user: User,
    ): Promise<Category[]> {
        const additionalWhere =
            option === IncludeInternalCategories.Include
                ? {}
                : { internal: false }

        return this.find({
            where: { userId: user.id, ...additionalWhere },
            order: { id: 'ASC' },
        })
    }

    getCategoryById(
        id: number,
        option: IncludeInternalCategories,
        user: User,
    ): Promise<Category> {
        const additionalWhere =
            option === IncludeInternalCategories.Include
                ? {}
                : { internal: false }

        return this.findOne({
            where: { id, userId: user.id, ...additionalWhere },
        })
    }

    async createCategory(
        createCategoryDto: CreateCategoryDto,
        user: User,
    ): Promise<Category> {
        const { name, type, iconName, color } = createCategoryDto

        const category = this.create()
        category.name = name
        category.type = type
        category.iconName = iconName
        category.color = color
        category.user = user
        await category.save()

        delete category.user // nie zwracaj danych użytkownika
        return category
    }

    deleteCategory(id: number, user: User): Promise<DeleteResult> {
        // nie pozwalaj usuwać wewnętrznych kategorii
        return this.delete({ id, userId: user.id, internal: false })
    }
}
