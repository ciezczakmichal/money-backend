import {
    BaseEntity,
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToOne,
    OneToMany,
} from 'typeorm'
import { CategoryType } from './category-type.enum'
import { User } from '../auth/user.entity'
import { ExternalTransaction } from '../transactions/transaction.entity'

@Entity('categories')
export class Category extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string

    @Column({ enum: CategoryType })
    type: CategoryType

    @Column({ nullable: true })
    iconName: string

    @Column({ nullable: true })
    color: string

    @Column({ default: false })
    archived: boolean

    @ManyToOne(
        _type => User,
        user => user.categories,
        { eager: false, onDelete: 'CASCADE' },
    )
    user: User

    @Column()
    userId: number

    @Column({ default: false })
    internal: boolean

    @Column({ nullable: true })
    internalName: string

    // używane w odpowiedziach
    amountForCurrentMonth: number

    /* Powiązania z transakcjami */

    @OneToMany(
        _type => ExternalTransaction,
        transaction => transaction.category,
        { eager: false },
    )
    externalTransactions: ExternalTransaction[]
}
