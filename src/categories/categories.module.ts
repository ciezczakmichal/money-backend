import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { CategoriesController } from './categories.controller'
import { CategoriesService } from './categories.service'
import { CategoryRepository } from './category.repository'
import { AuthModule } from '../auth/auth.module'
import { UtilitiesModule } from '../utilities/utilities.module'

@Module({
    imports: [
        TypeOrmModule.forFeature([CategoryRepository]),
        AuthModule,
        UtilitiesModule,
    ],
    controllers: [CategoriesController],
    providers: [CategoriesService],
    exports: [CategoriesService],
})
export class CategoriesModule {}
