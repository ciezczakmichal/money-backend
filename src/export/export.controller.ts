import { Controller, Get, Res, UseGuards } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { Response } from 'express'
import { GetUser } from '../auth/get-user.decorator'
import { User } from '../auth/user.entity'
import { ExportService, WriteCallback } from './export.service'

@Controller('export')
@UseGuards(AuthGuard())
export class ExportController {
    constructor(private exportService: ExportService) {}

    @Get('/accounts')
    async getExportedAccounts(
        @Res() res: Response,
        @GetUser() user: User,
    ): Promise<void> {
        await this.handleExport(
            res,
            user,
            'accounts',
            this.exportService.exportAccounts,
        )
    }

    @Get('/categories')
    async getExportedCategories(
        @Res() res: Response,
        @GetUser() user: User,
    ): Promise<void> {
        await this.handleExport(
            res,
            user,
            'categories',
            this.exportService.exportCategories,
        )
    }

    @Get('/transactions')
    async getExportedTransactions(
        @Res() res: Response,
        @GetUser() user: User,
    ): Promise<void> {
        await this.handleExport(
            res,
            user,
            'transactions',
            this.exportService.exportTransactions,
        )
    }

    private async handleExport(
        res: Response,
        user: User,
        filename: string,
        exportMethod: (
            user: User,
            writeCallback: WriteCallback,
        ) => Promise<void>,
    ): Promise<void> {
        res.contentType('text/csv; charset=utf-8')
        res.setHeader(
            'Content-Disposition',
            `attachment; filename=${filename}.csv`,
        )

        await exportMethod.bind(this.exportService)(user, (data: string) =>
            res.write(data),
        )

        res.end()
    }
}
