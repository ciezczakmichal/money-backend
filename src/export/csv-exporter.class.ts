interface Column<T> {
    name: string
    writer: (item: T) => string
}

export type WriteCallback = (data: string) => void

export class CsvExporter<T> {
    private columns: Column<T>[]
    private write: WriteCallback

    constructor() {
        this.columns = []
    }

    useColumn(name: string, writer: (item: T) => string): void {
        this.columns.push({
            name,
            writer,
        })
    }

    useColumnWhenClass<Y>(
        typeConstructor: { new (): Y },
        name: string,
        writer: (item: Y) => string,
    ): void {
        this.useColumn(name, (item: T): string => {
            if (item instanceof typeConstructor) {
                return writer(item)
            } else {
                return ''
            }
        })
    }

    useWriteCallback(writeCallback: WriteCallback): void {
        this.write = writeCallback
    }

    export(items: T[]): void {
        if (!this.write) {
            throw new Error('Nie przypisano funkcji zapisującej dane')
        }

        const writeLine = (line: string) => this.write(`${line}\r\n`)

        const header = this.columns.map(column => column.name).join(',')
        writeLine(header)

        for (const item of items) {
            const row = this.columns
                .map(column => column.writer(item))
                .join(',')

            writeLine(row)
        }
    }
}
