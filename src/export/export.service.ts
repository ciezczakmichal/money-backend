import { ModuleRef } from '@nestjs/core'
import { Injectable, OnModuleInit } from '@nestjs/common'
import { User } from '../auth/user.entity'
import { AccountsService } from '../accounts/accounts.service'
import {
    CategoriesService,
    IncludeInternalCategories,
} from '../categories/categories.service'
import { TransactionsService } from '../transactions/transactions.service'
import { Account } from '../accounts/account.entity'
import { Category } from '../categories/category.entity'
import {
    Transaction,
    AccountTransfer,
    ExternalTransaction,
} from '../transactions/transaction.entity'
import { CsvExporter, WriteCallback } from './csv-exporter.class'

export { WriteCallback } from './csv-exporter.class'

@Injectable()
export class ExportService implements OnModuleInit {
    private transactionsService: TransactionsService

    constructor(
        private accountsService: AccountsService,
        private categoriesService: CategoriesService,
        // TransactionsService nie może zostać odnaleziony w normalny sposób
        private moduleRef: ModuleRef,
    ) {}

    onModuleInit() {
        this.transactionsService = this.moduleRef.get(TransactionsService, {
            strict: false,
        })
    }

    async exportAccounts(
        user: User,
        writeCallback: WriteCallback,
    ): Promise<void> {
        const accounts = await this.accountsService.getAccounts(user)

        const csv = new CsvExporter<Account>()

        csv.useColumn('ID', account => `${account.id}`)
        csv.useColumn('Name', account => account.name)
        csv.useColumn('Description', account => account.description)
        csv.useColumn('Icon name', account => account.iconName)
        csv.useColumn('Default', account => `${account.default}`)
        csv.useColumn('Archived', account => `${account.archived}`)
        csv.useColumn('User ID', account => `${account.userId}`)
        csv.useColumn('Current balance', account => `${account.currentBalance}`)

        csv.useWriteCallback(writeCallback)
        csv.export(accounts)
    }

    async exportCategories(
        user: User,
        writeCallback: WriteCallback,
    ): Promise<void> {
        const categories = await this.categoriesService.getCategories(
            IncludeInternalCategories.Include,
            user,
        )

        const csv = new CsvExporter<Category>()

        csv.useColumn('ID', category => `${category.id}`)
        csv.useColumn('Name', category => category.name)
        csv.useColumn('Type', category => category.type)
        csv.useColumn('Icon name', category => category.iconName)
        csv.useColumn('Color', category => category.color)
        csv.useColumn('Archived', category => `${category.archived}`)
        csv.useColumn('User ID', category => `${category.userId}`)
        csv.useColumn(
            'Amount for current month',
            category => `${category.amountForCurrentMonth}`,
        )

        csv.useWriteCallback(writeCallback)
        csv.export(categories)
    }

    async exportTransactions(
        user: User,
        writeCallback: WriteCallback,
    ): Promise<void> {
        const transactions = await this.transactionsService.getAllTransactions(
            user,
        )

        const csv = new CsvExporter<Transaction>()

        csv.useColumn('ID', transaction => `${transaction.id}`)
        csv.useColumn('Amount', transaction => `${transaction.amount}`)
        csv.useColumn(
            'Effective amount',
            transaction => `${transaction.calculateEffectiveAmount()}`,
        )
        csv.useColumn('Date', transaction => transaction.date)
        csv.useColumn('Comment', transaction => transaction.comment)
        csv.useColumn('Type', transaction => transaction.type)
        csv.useColumnWhenClass(
            ExternalTransaction,
            'Account ID',
            transaction => `${transaction.accountId}`,
        )
        csv.useColumnWhenClass(
            ExternalTransaction,
            'Category ID',
            transaction => `${transaction.categoryId}`,
        )
        csv.useColumnWhenClass(
            AccountTransfer,
            'Source account ID',
            transaction => `${transaction.sourceAccountId}`,
        )
        csv.useColumnWhenClass(
            AccountTransfer,
            'Destination account ID',
            transaction => `${transaction.destinationAccountId}`,
        )

        csv.useWriteCallback(writeCallback)
        csv.export(transactions)
    }
}
