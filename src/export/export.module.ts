import { Module } from '@nestjs/common'
import { ExportService } from './export.service'
import { ExportController } from './export.controller'
import { AuthModule } from '../auth/auth.module'
import { AccountsModule } from '../accounts/accounts.module'
import { CategoriesModule } from '../categories/categories.module'
import { TransactionsModule } from '../transactions/transactions.module'

@Module({
    imports: [AuthModule, AccountsModule, CategoriesModule, TransactionsModule],
    providers: [ExportService],
    controllers: [ExportController],
})
export class ExportModule {}
