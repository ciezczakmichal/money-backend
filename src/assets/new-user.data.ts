import { CategoryType } from '../categories/category-type.enum'

export const AccountInitialCategoryInternalName = 'account_init_category'

export const NewUserData = {
    accounts: [
        {
            name: 'Karta',
            description: null,
            iconName: 'credit_card',
            default: true,
        },
        {
            name: 'Gotówka',
            description: null,
            iconName: 'account_balance_wallet',
            default: false,
        },
    ],
    categories: [
        {
            name: 'Transakcja inicjująca',
            type: CategoryType.Proceeds,
            iconName: 'tour',
            color: '#5c6bc0',
            internal: true,
            internalName: AccountInitialCategoryInternalName,
        },
        {
            name: 'Artykuły spożywcze',
            type: CategoryType.Expense,
            iconName: 'shopping_basket',
            color: '#03a9f4',
            internal: false,
            internalName: null,
        },
        {
            name: 'Restauracje',
            type: CategoryType.Expense,
            iconName: 'restaurant',
            color: '#5c6bc0',
            internal: false,
            internalName: null,
        },
        {
            name: 'Wypoczynek',
            type: CategoryType.Expense,
            iconName: 'theaters',
            color: '#e91e63',
            internal: false,
            internalName: null,
        },
        {
            name: 'Transport',
            type: CategoryType.Expense,
            iconName: 'directions_bus',
            color: '#ffc107',
            internal: false,
            internalName: null,
        },
        {
            name: 'Prezenty',
            type: CategoryType.Expense,
            iconName: 'card_giftcard',
            color: '#ff5722',
            internal: false,
            internalName: null,
        },
        {
            name: 'Zakupy',
            type: CategoryType.Expense,
            iconName: 'local_mall',
            color: '#795548',
            internal: false,
            internalName: null,
        },
        {
            name: 'Zdrowie',
            type: CategoryType.Expense,
            iconName: 'spa',
            color: '#00bcd4',
            internal: false,
            internalName: null,
        },
        {
            name: 'Rodzina',
            type: CategoryType.Expense,
            iconName: 'face',
            color: '#673ab7',
            internal: false,
            internalName: null,
        },
        {
            name: 'Wynagrodzenie',
            type: CategoryType.Proceeds,
            iconName: 'work_outline',
            color: '#009688',
            internal: false,
            internalName: null,
        },
    ],
}
