import { Injectable, OnModuleInit } from '@nestjs/common'
import { ModuleRef } from '@nestjs/core'
import { User } from '../auth/user.entity'
import { AccountsService } from '../accounts/accounts.service'
import {
    CategoriesService,
    IncludeInternalCategories,
} from '../categories/categories.service'
import { TransactionsService } from '../transactions/transactions.service'
import { TransactionType } from '../transactions/transaction-type.enum'
import { CreateAccountDto } from '../accounts/dto/create-account.dto'
import { CreateCategoryDto } from '../categories/dto/create-category.dto'
import { CreateTransactionDto } from '../transactions/dto/create-transaction.dto'
import {
    today,
    encodeDateComponents,
} from '../transactions/helpers/date-utils.helper'
import {
    NewUserData,
    AccountInitialCategoryInternalName,
} from '../assets/new-user.data'
import { TransactionsPerDay } from '../transactions/dto/transaction-list.dto'

@Injectable()
export class UtilitiesService implements OnModuleInit {
    private accountsService: AccountsService
    private categoriesService: CategoriesService
    private transactionsService: TransactionsService

    constructor(private moduleRef: ModuleRef) {}

    onModuleInit() {
        this.accountsService = this.moduleRef.get(AccountsService, {
            strict: false,
        })
        this.categoriesService = this.moduleRef.get(CategoriesService, {
            strict: false,
        })
        this.transactionsService = this.moduleRef.get(TransactionsService, {
            strict: false,
        })
    }

    async createBaseEntriesForUser(user: User): Promise<void> {
        /* Twórz elementy sekwencyjnie - daje to co prawda opóźnienie,
         * jednak gwarantuje, że elementy zostaną dodane do bazy
         * w takiej kolejności, jak zadeklarowano w pliku definicji */

        const { accounts, categories } = NewUserData

        const accountsCreator = async (): Promise<void> => {
            for (const account of accounts) {
                const { name, description, iconName } = account
                const dto: CreateAccountDto = {
                    name,
                    description,
                    iconName,
                    default: account.default,
                }

                await this.accountsService.createAccount(dto, user)
            }
        }

        const categoriesCreator = async (): Promise<void> => {
            for (const category of categories) {
                const { name, type, iconName, color } = category
                const dto: CreateCategoryDto = {
                    name,
                    type,
                    iconName,
                    color,
                }

                const created = await this.categoriesService.createCategory(
                    dto,
                    user,
                )

                if (category.internal) {
                    await this.categoriesService.markCategoryAsInternal(
                        created.id,
                        category.internalName,
                        user,
                    )
                }
            }
        }

        await Promise.all([accountsCreator(), categoriesCreator()])
    }

    async createAccountInitialTransaction(
        accountId: number,
        amount: number,
        user: User,
    ): Promise<void> {
        const categories = await this.categoriesService.getCategories(
            IncludeInternalCategories.Include,
            user,
        )
        const category = categories.find(
            category =>
                category.internal &&
                category.internalName === AccountInitialCategoryInternalName,
        )

        if (!category) {
            console.warn(
                `Nie znaleziono kategorii pozwalającej dodać inicjującą ` +
                    `transakcję dla konta. ID użytkownika: ${user.id}`,
            )
            return
        }

        const dto: CreateTransactionDto = {
            amount,
            date: encodeDateComponents(today()),
            comment: null,
            type: TransactionType.ExternalTransaction,
            accountId,
            categoryId: category.id,
            sourceAccountId: null,
            destinationAccountId: null,
        }

        await this.transactionsService.createTransaction(
            dto,
            user,
            IncludeInternalCategories.Include,
        )
    }

    getTransactionsForCategory(
        categoryId: number,
        user: User,
    ): Promise<TransactionsPerDay[]> {
        return this.transactionsService.getTransactionsForCategory(
            categoryId,
            user,
        )
    }

    recalculateTransactions(user: User): Promise<void> {
        return this.transactionsService.recalculateTransactions(user)
    }

    getAccountBalance(accountId: number, user: User): Promise<number> {
        return this.transactionsService.getAccountBalance(accountId, user)
    }

    async getCategoryTotalAmountForMonth(
        categoryId: number,
        user: User,
    ): Promise<number> {
        return this.transactionsService.getCategoryTotalAmountForCurrentMonth(
            categoryId,
            user,
        )
    }
}
