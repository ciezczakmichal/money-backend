import { Module } from '@nestjs/common'
import { UtilitiesService } from './utilities.service'

@Module({
    // AccountsService, CategoriesService, TransactionsService wymagane niejawnie
    providers: [UtilitiesService],
    exports: [UtilitiesService],
})
export class UtilitiesModule {}
