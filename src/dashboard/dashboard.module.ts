import { Module } from '@nestjs/common'
import { DashboardController } from './dashboard.controller'
import { DashboardService } from './dashboard.service'
import { AuthModule } from '../auth/auth.module'
import { CategoriesModule } from '../categories/categories.module'
import { TransactionsModule } from '../transactions/transactions.module'

@Module({
    imports: [AuthModule, CategoriesModule, TransactionsModule],
    controllers: [DashboardController],
    providers: [DashboardService],
})
export class DashboardModule {}
