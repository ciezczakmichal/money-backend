import { Controller, UseGuards, Get } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { DashboardService } from './dashboard.service'
import { GetUser } from '../auth/get-user.decorator'
import { User } from '../auth/user.entity'
import { DashboardResponse } from './dashboard-response.interface'

@Controller('dashboard')
@UseGuards(AuthGuard())
export class DashboardController {
    constructor(private dashboardService: DashboardService) {}

    @Get()
    getData(@GetUser() user: User): Promise<DashboardResponse> {
        return this.dashboardService.getData(user)
    }
}
