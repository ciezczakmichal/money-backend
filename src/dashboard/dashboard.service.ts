import { Injectable, OnModuleInit } from '@nestjs/common'
import { ModuleRef } from '@nestjs/core'
import { User } from '../auth/user.entity'
import {
    CategoriesService,
    IncludeInternalCategories,
    IncludeAmountCalculation,
} from '../categories/categories.service'

import {
    DashboardResponse,
    CategoryStatistics,
    DiagramData,
    DiagramCategoryData,
} from './dashboard-response.interface'
import { Transaction } from '../transactions/transaction.entity'
import { TransactionsService } from '../transactions/transactions.service'
import { CategoryType } from '../categories/category-type.enum'
import {
    getDaysInCurrentMonth,
    today,
} from '../transactions/helpers/date-utils.helper'

const LastTransactionsCount = 10

@Injectable()
export class DashboardService implements OnModuleInit {
    private transactionsService: TransactionsService

    constructor(
        private categoriesService: CategoriesService,
        private moduleRef: ModuleRef,
    ) {}

    onModuleInit() {
        this.transactionsService = this.moduleRef.get(TransactionsService, {
            strict: false,
        })
    }

    async getData(user: User): Promise<DashboardResponse> {
        const expensesData = this.getDiagramData(CategoryType.Expense, user)
        const proceedsData = this.getDiagramData(CategoryType.Proceeds, user)
        const categoriesStatistics = this.getCategoriesStatistics(user)
        const transactions = this.getLastTransactions(user)

        return {
            expensesDiagramData: await expensesData,
            proceedsDiagramData: await proceedsData,
            categoriesStats: await categoriesStatistics,
            lastTransactions: await transactions,
        }
    }

    async getDiagramData(
        categoryType: CategoryType,
        user: User,
    ): Promise<DiagramData> {
        const daysCount = getDaysInCurrentMonth()

        const labels: string[] = await this.generateMonthArray(
            daysCount,
            day => `${day}`,
        )

        const allCategories = await this.categoriesService.getCategories(
            IncludeInternalCategories.Exclude,
            user,
            IncludeAmountCalculation.Include,
        )

        // zwróć dane tylko dla kategori, które pojawiły się w miesiącu
        const categories = allCategories.filter(
            category =>
                category.type === categoryType &&
                category.amountForCurrentMonth !== 0,
        )

        const { month, year } = today()
        const datasets: DiagramCategoryData[] = []

        for (const category of categories) {
            const data: number[] = await this.generateMonthArray<number>(
                daysCount,
                day =>
                    this.transactionsService.getCategoryTotalAmountForDate(
                        category.id,
                        { year, month, day },
                        user,
                    ),
            )

            datasets.push({
                label: category.name,
                backgroundColor: category.color,
                data,
            })
        }

        return { labels, datasets }
    }

    async getCategoriesStatistics(user: User): Promise<CategoryStatistics[]> {
        let categories = await this.categoriesService.getCategories(
            IncludeInternalCategories.Exclude,
            user,
            IncludeAmountCalculation.Include,
        )

        // zwróć tylko wydatki
        categories = categories.filter(
            category => category.type === CategoryType.Expense,
        )

        // posortuj wg kwoty w miesiącu
        categories = categories.sort(
            (a, b) => b.amountForCurrentMonth - a.amountForCurrentMonth,
        )

        // oblicz sumę
        let total = 0
        categories.forEach(
            category => (total += category.amountForCurrentMonth),
        )

        return categories.map(category => {
            const { amountForCurrentMonth } = category
            const percent =
                total > 0 ? (amountForCurrentMonth / total) * 100 : 0

            return {
                category,
                percent: Math.round(percent),
                amount: amountForCurrentMonth,
            }
        })
    }

    async getLastTransactions(user: User): Promise<Transaction[]> {
        const transactions = await this.transactionsService.getAllTransactions(
            user,
        )

        return transactions.slice(0, LastTransactionsCount)
    }

    private async generateMonthArray<T>(
        days: number,
        generator: ((day: number) => T) | ((day: number) => Promise<T>),
    ): Promise<T[]> {
        let currentDay = 1
        const result: T[] = []

        while (currentDay <= days) {
            const value = await generator(currentDay)
            result.push(value)
            currentDay++
        }

        return result
    }
}
