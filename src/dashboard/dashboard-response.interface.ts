import { Category } from '../categories/category.entity'
import { Transaction } from '../transactions/transaction.entity'

export interface DiagramCategoryData {
    // nazwa kategorii
    label: string

    // kolor kategorii
    backgroundColor: string

    // wartości transakcji danej kategorii dla poszczególnych dni miesiąca
    data: number[]
}

export interface DiagramData {
    // etykiety osi - numery dni w miesiącu
    labels: string[]

    // dane poszczególnych kategorii
    datasets: DiagramCategoryData[]
}

export interface CategoryStatistics {
    category: Category
    percent: number
    amount: number
}

export interface DashboardResponse {
    expensesDiagramData: DiagramData
    proceedsDiagramData: DiagramData
    categoriesStats: CategoryStatistics[]
    lastTransactions: Transaction[]
}
