import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { TypeOrmConfigService } from './config/typeorm-config.service'
import { ConfigModule } from '@nestjs/config'
import configuration from './config/configuration'

import { AuthModule } from './auth/auth.module'
import { AccountsModule } from './accounts/accounts.module'
import { CategoriesModule } from './categories/categories.module'
import { UtilitiesModule } from './utilities/utilities.module'
import { TransactionsModule } from './transactions/transactions.module'
import { ExportModule } from './export/export.module'
import { DashboardModule } from './dashboard/dashboard.module'

@Module({
    imports: [
        ConfigModule.forRoot({ load: [configuration] }),
        TypeOrmModule.forRootAsync({
            imports: [ConfigModule],
            useClass: TypeOrmConfigService,
        }),
        AuthModule,
        AccountsModule,
        CategoriesModule,
        TransactionsModule,
        UtilitiesModule,
        ExportModule,
        DashboardModule,
    ],
})
export class AppModule {}
