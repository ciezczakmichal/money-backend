import { Repository, EntityRepository } from 'typeorm'
import {
    Transaction,
    ExternalTransaction,
    AccountTransfer,
} from './transaction.entity'
import { User } from '../auth/user.entity'
import { CreateTransactionDto } from './dto/create-transaction.dto'
import { TransactionType } from './transaction-type.enum'

@EntityRepository(Transaction)
export class TransactionRepository extends Repository<Transaction> {
    // zwracane obiekty nie posiadają danych z tabel powiązanych relacją
    // daty graniczne są włączane
    // transakcje posortowane są malejąco według dat oraz id
    async getTransactions(
        user: User,
        period?: {
            fromDate: string
            toDate: string
        },
    ): Promise<Transaction[]> {
        const query = this.createQueryBuilder('transaction')

        query.leftJoin(
            'accounts',
            'account1',
            'transaction.accountId = account1.id',
        )
        query.leftJoin(
            'categories',
            'category',
            'transaction.categoryId = category.id',
        )

        query.leftJoin(
            'accounts',
            'account2',
            'transaction.sourceAccountId = account2.id',
        )
        query.leftJoin(
            'accounts',
            'account3',
            'transaction.destinationAccountId = account3.id',
        )

        // wymagaj prawa do posiadania obiektów relacji
        query.where(
            '(((account1.userId = :userId) AND (category.userId = :userId)) ' +
                'OR ((account2.userId = :userId) AND (account3.userId = :userId)))',
            { userId: user.id },
        )

        // jeśli zadano, zwracaj dane ze wskazanego okresu czasowego
        if (period) {
            query.andWhere(
                '(transaction.date >= :fromDate AND transaction.date < :toDate)',
                { fromDate: period.fromDate, toDate: period.toDate },
            )
        }

        query.orderBy('transaction.date', 'DESC')
        query.addOrderBy('transaction.id', 'DESC')

        const transactions = await query.getMany()
        return transactions
    }

    async getTransactionById(id: number, user: User): Promise<Transaction> {
        let found = await this.findOne({
            where: { id },
        })

        if (found && !found.isOwnedByUser(user)) {
            found = null
        }

        return found
    }

    async createTransaction(
        createTransactionDto: CreateTransactionDto,
    ): Promise<Transaction> {
        const { amount, date, comment, type } = createTransactionDto

        const transaction = this.createTransactionInstance(type)
        transaction.amount = amount
        transaction.date = date
        transaction.comment = comment

        if (transaction instanceof ExternalTransaction) {
            transaction.accountId = createTransactionDto.accountId
            transaction.categoryId = createTransactionDto.categoryId
        } else if (transaction instanceof AccountTransfer) {
            transaction.sourceAccountId = createTransactionDto.sourceAccountId
            transaction.destinationAccountId =
                createTransactionDto.destinationAccountId
        }

        await transaction.save()
        return transaction
    }

    private createTransactionInstance(
        transactionType: TransactionType,
    ): Transaction {
        switch (transactionType) {
            case TransactionType.ExternalTransaction:
                return new ExternalTransaction()

            case TransactionType.AccountTransfer:
                return new AccountTransfer()

            default:
                throw new Error(
                    `Nierozpoznany typ transakcji "${transactionType}"`,
                )
        }
    }

    async deleteTransaction(id: number, user: User): Promise<boolean> {
        const transaction = await this.getTransactionById(id, user)
        const valid = transaction instanceof Transaction

        if (valid) {
            await this.remove(transaction)
        }

        return valid
    }
}
