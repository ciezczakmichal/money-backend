import { Transaction } from '../transaction.entity'
import { Account } from '../../accounts/account.entity'
import { Category } from '../../categories/category.entity'

export enum PeriodName {
    Month = 'MONTH',
}

export interface Period {
    name: PeriodName
    from: string
    to: string
}

export interface TransactionsPerDay {
    date: string
    year: number
    month: number
    day: number
    totalEffectiveAmount: number
    transactions: Transaction[]
}

export interface TransactionListDto {
    period: Period
    startBalance: number
    endBalance: number
    data: TransactionsPerDay[]
    accounts: Account[]
    categories: Category[]
}
