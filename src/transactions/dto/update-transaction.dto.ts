import { BaseTransactionDto } from './base-transaction.dto'
import { IsOptional } from 'class-validator'
import { TransactionType } from '../transaction-type.enum'

export class UpdateTransactionDto extends BaseTransactionDto {
    /* Przeładowane właściwości z klasy bazowej */

    @IsOptional()
    amount: number

    @IsOptional()
    date: string

    @IsOptional()
    comment: string

    @IsOptional()
    type: TransactionType

    @IsOptional()
    accountId: number

    @IsOptional()
    categoryId: number

    @IsOptional()
    sourceAccountId: number

    @IsOptional()
    destinationAccountId: number

    // brak zmian w liście właściwości
}
