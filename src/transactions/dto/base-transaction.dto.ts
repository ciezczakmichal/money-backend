import {
    IsNotEmpty,
    IsOptional,
    IsString,
    IsEnum,
    IsNumber,
    Matches,
    ValidateIf,
    IsInt,
    NotEquals,
} from 'class-validator'
import { TransactionType } from '../transaction-type.enum'
import {
    DateFormatRegex,
    DateFormatString,
} from '../helpers/date-constants.helper'

export class BaseTransactionDto {
    @IsNotEmpty()
    @IsNumber(
        { allowInfinity: false, allowNaN: false, maxDecimalPlaces: 2 },
        {
            message:
                'Kwota powinna być liczbą z maksymalnie 2 miejscami po przecinku',
        },
    )
    @NotEquals(0, { message: 'Kwota powinna być różna od zera' })
    amount: number

    // @todo pełne sprawdzanie wartości daty, np. 2020-06-31
    @IsNotEmpty()
    @Matches(DateFormatRegex, {
        message: `Niepoprawny format lub wartość daty (oczekiwano ${DateFormatString})`,
    })
    date: string

    @IsOptional()
    @IsString()
    comment: string

    @IsEnum(TransactionType, {
        message: 'Niezdefiniowany lub nieprawidłowy typ transakcji',
    })
    type: TransactionType

    @ValidateIf(t => t.type === TransactionType.ExternalTransaction)
    @IsInt()
    accountId: number

    @ValidateIf(t => t.type === TransactionType.ExternalTransaction)
    @IsInt()
    categoryId: number

    @ValidateIf(t => t.type === TransactionType.AccountTransfer)
    @IsInt()
    sourceAccountId: number

    @ValidateIf(t => t.type === TransactionType.AccountTransfer)
    @IsInt()
    destinationAccountId: number

    static allDtoProperties = [
        'amount',
        'date',
        'comment',
        'type',
        'accountId',
        'categoryId',
        'sourceAccountId',
        'destinationAccountId',
    ]
}
