import {
    Controller,
    UseGuards,
    Get,
    Param,
    ParseIntPipe,
    Post,
    UsePipes,
    ValidationPipe,
    Body,
    Put,
    Delete,
    Query,
} from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { TransactionsService } from './transactions.service'
import { GetUser } from '../auth/get-user.decorator'
import { User } from '../auth/user.entity'
import { Transaction } from './transaction.entity'
import { CreateTransactionDto } from './dto/create-transaction.dto'
import { UpdateTransactionDto } from './dto/update-transaction.dto'
import { GetTransactionsFilterDto } from './dto/get-transactions-filter.dto'
import { GetTransactionsFilterValidationPipe } from './pipes/get-transactions-filter-validation.pipe'
import { TransactionListDto } from './dto/transaction-list.dto'

@Controller('transactions')
@UseGuards(AuthGuard())
export class TransactionsController {
    constructor(private transactionsService: TransactionsService) {}

    @Get()
    getTransactions(
        @GetUser() user: User,
        @Query(GetTransactionsFilterValidationPipe)
        filterDto: GetTransactionsFilterDto,
    ): Promise<TransactionListDto> {
        return this.transactionsService.getTransactions(user, filterDto)
    }

    @Post()
    @UsePipes(ValidationPipe)
    createCategory(
        @Body() createTransactionDto: CreateTransactionDto,
        @GetUser() user: User,
    ): Promise<Transaction> {
        return this.transactionsService.createTransaction(
            createTransactionDto,
            user,
        )
    }

    @Get('/:id')
    getTransactionById(
        @Param('id', ParseIntPipe) id: number,
        @GetUser() user: User,
    ): Promise<Transaction> {
        return this.transactionsService.getTransactionById(id, user)
    }

    @Put('/:id')
    @UsePipes(ValidationPipe)
    updateTransaction(
        @Param('id', ParseIntPipe) id: number,
        @Body() updateTransactionDto: UpdateTransactionDto,
        @GetUser() user: User,
    ): Promise<Transaction> {
        return this.transactionsService.updateTransaction(
            id,
            updateTransactionDto,
            user,
        )
    }

    @Delete('/:id')
    deleteTransaction(
        @Param('id', ParseIntPipe) id: number,
        @GetUser() user: User,
    ): Promise<void> {
        return this.transactionsService.deleteTransaction(id, user)
    }
}
