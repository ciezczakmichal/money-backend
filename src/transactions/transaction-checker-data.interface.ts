import { TransactionType } from './transaction-type.enum'
import { CreateTransactionDto } from './dto/create-transaction.dto'
import {
    Transaction,
    ExternalTransaction,
    AccountTransfer,
} from './transaction.entity'

export class TransactionForeignKeysCheckerData {
    type: TransactionType

    accountId: number
    categoryId: number

    sourceAccountId: number
    destinationAccountId: number

    static fromCreateTransactionDto(
        dto: CreateTransactionDto,
    ): TransactionForeignKeysCheckerData {
        const obj = new TransactionForeignKeysCheckerData()
        obj.type = dto.type
        obj.accountId = dto.accountId
        obj.categoryId = dto.categoryId
        obj.sourceAccountId = dto.sourceAccountId
        obj.destinationAccountId = dto.destinationAccountId
        return obj
    }

    static fromTransaction(
        transaction: Transaction,
    ): TransactionForeignKeysCheckerData {
        const obj = new TransactionForeignKeysCheckerData()
        obj.type = transaction.type

        if (transaction instanceof ExternalTransaction) {
            obj.accountId = transaction.accountId
            obj.categoryId = transaction.categoryId
        } else if (transaction instanceof AccountTransfer) {
            obj.sourceAccountId = transaction.sourceAccountId
            obj.destinationAccountId = transaction.destinationAccountId
        }

        return obj
    }
}
