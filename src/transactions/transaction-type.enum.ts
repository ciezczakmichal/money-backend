// rodzaj transakcji - przychód / wydatek lub transfer między kontami
export enum TransactionType {
    ExternalTransaction = 'EXTERNAL_TRANSACTION',
    AccountTransfer = 'ACCOUNT_TRANSFER',
}
