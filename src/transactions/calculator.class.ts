import {
    Transaction,
    ExternalTransaction,
    AccountTransfer,
} from './transaction.entity'
import {
    DateComponents,
    decodeDateComponents,
} from './helpers/date-utils.helper'
import { SortedArray } from '../libs/sorted-array'
import { isDateAfter } from './helpers/date-utils.helper'

/**
 * Zwraca element, który jako ostatni przechodzi test lub NULL
 * Tablica powinna być posortowana rosnąco.
 * @param items
 * @param predicate
 */
function findLastItem<T>(sortedArray: T[], predicate: (item: T) => boolean): T {
    let result: T = null

    for (const item of sortedArray) {
        if (predicate(item)) {
            result = item
        } else {
            break
        }
    }

    return result
}

const fixNumber = (num: number): number => parseFloat(num.toFixed(2))

declare type AccountId = number
declare type CategoryId = number

/* Przechowuje stan kont na dzień */
class DayData {
    day: number
    balances: Map<AccountId, number>
    categoriesAmount: Map<CategoryId, number>

    constructor(day: number, balancesToCopy?: Map<AccountId, number>) {
        this.day = day

        if (balancesToCopy) {
            this.balances = new Map(balancesToCopy)
        } else {
            this.balances = new Map<AccountId, number>()
        }

        this.categoriesAmount = new Map<CategoryId, number>()
    }
}

class MonthData {
    month: number
    days: SortedArray<DayData>
    categoriesAmount: Map<CategoryId, number> // ID kategorii i wartość

    constructor(month: number) {
        this.month = month
        this.days = new SortedArray<DayData>([], 'day')
        this.categoriesAmount = new Map<CategoryId, number>()
    }

    findDay(day: number): DayData {
        return this.days.find(dayData => dayData.day === day)
    }
}

class YearData {
    year: number
    months: SortedArray<MonthData>

    constructor(year: number) {
        this.year = year
        this.months = new SortedArray<MonthData>([], 'month')
    }

    findMonth(month: number): MonthData {
        return this.months.find(monthData => monthData.month === month)
    }
}

export class Calculator {
    private accounts: AccountId[] // ID kont, dla których pojawiły się transakcje
    private categories: CategoryId[] // ID kategorii, które pojawiły się w transakcjach
    private data: SortedArray<YearData>
    private latestDate: DateComponents // najnowsza, najmłodsza data

    constructor() {
        this.accounts = []
        this.categories = []
        this.data = new SortedArray<YearData>([], 'year')
        this.latestDate = null
    }

    /**
     * Dodaje transakcję do przetworzenia.
     * Wspierane implementacje transakcji to: ExternalTransaction, AccountTransfer.
     * Dla innych typów rzucane są wyjątki.
     * @param transaction Transakcja do dodania
     */
    addTransaction(transaction: Transaction): void {
        const { date, amount } = transaction
        const dateComponents = decodeDateComponents(date)
        const { year, month } = dateComponents

        let dayData = this.tryGetDayData(dateComponents)

        if (!dayData) {
            dayData = this.createDayDataEntry(dateComponents)
        }

        if (transaction instanceof ExternalTransaction) {
            const { accountId, categoryId } = transaction

            // obsługa nowego konta
            this.addAccountIfNecessary(accountId)

            // obsługa nowej kategorii
            if (!this.categoryInTransactions(categoryId)) {
                this.categories.push(categoryId)
            }

            const monthData = this.tryGetMonthData(year, month)

            // zaktualizuj kwotę kategorii w dniu
            let categAmount = dayData.categoriesAmount.get(categoryId) || 0
            categAmount += transaction.amount
            dayData.categoriesAmount.set(categoryId, categAmount)

            // zaktualizuj kwotę kategorii w miesiącu
            const categoryAmount =
                monthData.categoriesAmount.get(categoryId) || 0
            monthData.categoriesAmount.set(categoryId, categoryAmount + amount)

            // zmień saldo konta od dnia tranksacji
            this.applyFromDate(dateComponents, (dayData: DayData) => {
                let balance = dayData.balances.get(accountId) || 0

                // zmień bilans konta o kwotę transakcji
                balance += transaction.calculateEffectiveAmount()

                dayData.balances.set(accountId, balance)
            })
        } else if (transaction instanceof AccountTransfer) {
            const { sourceAccountId, destinationAccountId } = transaction

            // obsługa nowych kont
            this.addAccountIfNecessary(sourceAccountId)
            this.addAccountIfNecessary(destinationAccountId)

            // zmień saldo kont od dnia tranksacji
            this.applyFromDate(dateComponents, (dayData: DayData) => {
                const { balances } = dayData

                let sourceBalance = balances.get(sourceAccountId) || 0
                let destinationBalance = balances.get(destinationAccountId) || 0

                // zmień odpowiednio bilans kont
                sourceBalance -= amount
                destinationBalance += amount

                balances.set(sourceAccountId, sourceBalance)
                balances.set(destinationAccountId, destinationBalance)
            })
        } else {
            throw new Error(`Niewspierany typ transakcji`)
        }

        if (!this.latestDate || isDateAfter(this.latestDate, dateComponents)) {
            this.latestDate = dateComponents
        }
    }

    /**
     * Dodaje transakcje do przetworzenia.
     * Wspierane implementacje transakcji to: ExternalTransaction, AccountTransfer.
     * Dla innych typów rzucane są wyjątki.
     * @param transactions Transakcje do dodania
     */
    addTransactions(transactions: Transaction[]): void {
        transactions.forEach(transaction => this.addTransaction(transaction))
    }

    /**
     * Zwraca informację, czy z kontem powiązana została przynajmniej jedna transakcja.
     * @param accountId ID konta do sprawdzenia
     */
    accountHasTransactions(accountId: number): boolean {
        return this.accounts.findIndex(id => id === accountId) > -1
    }

    /**
     * Zwraca informację, czy kategoria pojawiła się w transakcjach.
     * @param categoryId
     */
    categoryInTransactions(categoryId: number): boolean {
        return this.categories.findIndex(id => id === categoryId) > -1
    }

    /**
     * Rzuca wyjątek, jeśli konto nie istnieje.
     * @param accountId
     */
    queryAccountBalance(accountId: number): number {
        if (!this.latestDate) {
            throw new Error(
                'Wyliczenia nie są dostępne, nie przekazano żadnych transakcji',
            )
        }

        return this.queryAccountBalanceAtEndOfDay(accountId, this.latestDate)
    }

    /**
     * Rzuca wyjątek, jeśli konto nie istnieje.
     * @param accountId
     * @param date
     */
    queryAccountBalanceAtEndOfDay(
        accountId: number,
        date: DateComponents,
    ): number {
        if (!this.accountHasTransactions(accountId)) {
            throw new Error(
                `Konto o ID "${accountId}" nie wystąpiło w przekazanych transakcjach`,
            )
        }

        const dayData = this.findNearestPreviousOrSameDay(date)

        // okres wykracza datę pierwszej transakcji
        if (!dayData) {
            return 0
        }

        /* Jeśli konto nie występuje, ale na pewno jest zarejestrowane,
         * oznacza to, że w tym okresie nie posiada jeszcze dotykających je transakcji,
         * zatem jego bilans wynosi 0 */
        return fixNumber(dayData.balances.get(accountId) || 0)
    }

    queryTotalAccountsBalance(): number {
        if (!this.latestDate) {
            return 0
        }

        return this.queryTotalAccountsBalanceAtEndOfDay(this.latestDate)
    }

    queryTotalAccountsBalanceAtEndOfDay(date: DateComponents): number {
        let value = 0

        for (const accountId of this.accounts) {
            value += this.queryAccountBalanceAtEndOfDay(accountId, date)
        }

        return fixNumber(value)
    }

    /**
     * Rzuca wyjątek, jeśli kategoria nie została zarejestrowana.
     * @param categoryId
     * @param date
     */
    queryCategoryTotalAmountForDate(
        categoryId: number,
        date: DateComponents,
    ): number {
        if (!this.categoryInTransactions(categoryId)) {
            throw new Error(
                `Kategoria o ID "${categoryId}" nie wystąpiła w przekazanych transakcjach`,
            )
        }

        const dayData = this.tryGetDayData(date)

        if (!dayData) {
            return 0
        }

        // we wskazanym dniu kategoria mogła nie wystąpić
        return fixNumber(dayData.categoriesAmount.get(categoryId) || 0)
    }

    /**
     * Rzuca wyjątek, jeśli kategoria nie została zarejestrowana.
     * @param categoryId
     * @param year
     * @param month
     */
    queryCategoryTotalAmountForMonth(
        categoryId: number,
        year: number,
        month: number,
    ): number {
        if (!this.categoryInTransactions(categoryId)) {
            throw new Error(
                `Kategoria o ID "${categoryId}" nie wystąpiła w przekazanych transakcjach`,
            )
        }

        const monthData = this.tryGetMonthData(year, month)

        if (!monthData) {
            return 0
        }

        // we wskazanym miesiącu kategoria mogła nie wystąpić
        return fixNumber(monthData.categoriesAmount.get(categoryId) || 0)
    }

    /* Metody prywatne */

    private addAccountIfNecessary(accountId: number): void {
        if (!this.accountHasTransactions(accountId)) {
            this.accounts.push(accountId)
        }
    }

    private tryGetYearData(year: number): YearData {
        return this.data.find(yearData => yearData.year === year) || null
    }

    private tryGetMonthData(year: number, month: number): MonthData {
        const yearData = this.tryGetYearData(year)

        if (!yearData) {
            return null
        } else {
            return yearData.findMonth(month) || null
        }
    }

    /** Zwraca dane dnia lub null, jeżeli wpis z konkretnym dniem nie występuje */
    private tryGetDayData(date: DateComponents): DayData {
        const { year, month, day } = date

        const monthData = this.tryGetMonthData(year, month)

        if (!monthData) {
            return null
        } else {
            return monthData.findDay(day) || null
        }
    }

    /** Nie sprawdza, czy wpis dla dnia istnieje.
     * Tworzy automatycznie rok i miesiąc, jeśli trzeba */
    private createDayDataEntry(date: DateComponents): DayData {
        const prevDayData = this.findNearestPreviousOrSameDay(date)

        const { year, month, day } = date

        let yearData = this.tryGetYearData(year)

        if (!yearData) {
            yearData = new YearData(year)
            this.data.insert(yearData)
        }

        let monthData = yearData.findMonth(month)

        if (!monthData) {
            monthData = new MonthData(month)
            yearData.months.insert(monthData)
        }

        let dayData: DayData

        // nowe wpisy dni należy tworzyć na podstawie najbliższego wpisu w przeszłości
        if (prevDayData) {
            dayData = new DayData(day, prevDayData.balances)
        } else {
            dayData = new DayData(day)
        }

        monthData.days.insert(dayData)

        return dayData
    }

    /**
     * Zwraca dane wskazanego dnia lub dnia będącego najbliżej
     * w przeszłości. Gdyby nie było takich danych, metoda zwraca NULL.
     * @param date
     */
    private findNearestPreviousOrSameDay(date: DateComponents): DayData {
        const MaxDaysInMonth = 31 // maksymalna liczba dni w miesiącu
        let { year, month, day } = date

        while (true) {
            const yearData = findLastItem(
                this.data.getArray(),
                data => data.year <= year,
            )

            // okres wykracza datę pierwszej transakcji
            if (!yearData) {
                return null
            }

            // jeśli znaleźliśmy dane dla wcześniejszego roku, należy przeszukiwać od jego końca
            if (yearData.year !== year) {
                month = 12
                day = MaxDaysInMonth
            }

            const monthData = findLastItem(
                yearData.months.getArray(),
                data => data.month <= month,
            )

            // w obecnym roku nie znaleziono oczekiwanych wpisów dla miesięcy, trzeba się cofnąć
            if (!monthData) {
                year--
                month = 12
                day = MaxDaysInMonth
                continue
            }

            // jeśli znaleźliśmy dane dla wcześniejszego miesiąca, należy przeszukiwać od jego końca
            if (monthData.month !== month) {
                day = MaxDaysInMonth
            }

            const dayData = findLastItem(
                monthData.days.getArray(),
                data => data.day <= day,
            )

            // w obecnym miesiącu nie znaleziono oczekiwanych wpisów dla dni, trzeba się cofnąć
            // można zoptymalizować
            if (!dayData) {
                month--
                day = MaxDaysInMonth
                continue
            }

            return dayData
        }
    }

    /**
     * Wykonuje iterację na wszystkich obiektach dni począwszy od dnia przekazanego
     * jako parametr, wywołując na nich przekazaną funkcję.
     * @param date
     * @param transformer
     */
    private applyFromDate(
        date: DateComponents,
        transformer: (dayData: DayData) => void,
    ) {
        let { year, month, day } = date

        while (true) {
            const yearData = this.data.find(data => data.year >= year)

            // wyjście poza dzień ostatniej transakcji
            if (!yearData) {
                break
            }

            // przypisz faktycznie znaleziony rok (nie używaj wielokrotnie)
            year = yearData.year

            const monthData = yearData.months.find(data => data.month >= month)

            // w obecnym roku nie znaleziono już wpisów dla miesięcy, trzeba zmienić rok
            if (!monthData) {
                year++
                month = 1
                day = 1
                continue
            }

            // przypisz faktycznie znaleziony miesiąc (nie używaj wielokrotnie)
            month = monthData.month

            // dni, dla których zastosować przekazaną funkcję
            let daysData: DayData[] = []

            if (day === 1) {
                // nie ma sensu filtrować tablicy, jeśli iterujemy przez wszystkie dni
                daysData = monthData.days.getArray()
            } else {
                daysData = monthData.days
                    .getArray()
                    .filter(data => data.day >= day)
            }

            daysData.forEach(dayData => transformer(dayData))

            month++
            day = 1
        }
    }
}
