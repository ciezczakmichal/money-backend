import {
    Entity,
    BaseEntity,
    PrimaryGeneratedColumn,
    Column,
    TableInheritance,
    ChildEntity,
    ManyToOne,
} from 'typeorm'
import { TransactionType } from './transaction-type.enum'
import { Account } from '../accounts/account.entity'
import { Category } from '../categories/category.entity'
import { User } from '../auth/user.entity'
import { ColumnNumericTransformer } from './numeric-column.transformer'
import { CategoryType } from '../categories/category-type.enum'

const NoRelationData = 'Brak danych z tabeli zależnej do wykonania metody'

@Entity('transactions')
@TableInheritance({
    column: { type: 'varchar', name: 'type', enum: TransactionType },
})
export abstract class Transaction extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column({
        type: 'numeric',
        precision: 18,
        scale: 2,
        transformer: new ColumnNumericTransformer(),
    })
    amount: number

    @Column({
        type: 'date',
        // transformer: {
        //     from: (value: string) => new Date(value),
        //     to: (value: Date) => value.getUTCDate(),
        // },
    })
    date: string
    //date: Date

    @Column({ nullable: true })
    comment: string

    @Column({ enum: TransactionType })
    type: TransactionType

    abstract isOwnedByUser(user: User): boolean

    abstract calculateEffectiveAmount(): number

    /** Właściwość zawierająca ostatnie wyliczenie metody calculateEffectiveAmount().
     * Dodana w celu zawarcia tej wartości w odpowiedzi w API (gdy potrzebne). */
    protected effectiveAmount: number
}

@ChildEntity(TransactionType.ExternalTransaction)
export class ExternalTransaction extends Transaction {
    // przypisz właściwy typ po utworzeniu obiektu, nie czekaj na zapisanie w bazie
    constructor() {
        super()
        this.type = TransactionType.ExternalTransaction
    }

    @ManyToOne(
        _type => Account,
        account => account.externalTransactions,
        { eager: true, onDelete: 'CASCADE' },
    )
    account: Account

    @Column()
    accountId: number

    @ManyToOne(
        _type => Category,
        category => category.externalTransactions,
        { eager: true, onDelete: 'CASCADE' },
    )
    category: Category

    @Column()
    categoryId: number

    isOwnedByUser(user: User): boolean {
        if (!this.account || !this.category) {
            throw new Error(NoRelationData)
        }

        return (
            this.account.userId === user.id && this.category.userId === user.id
        )
    }

    calculateEffectiveAmount(): number {
        if (!this.category) {
            throw new Error(NoRelationData)
        }

        const factor = this.category.type === CategoryType.Expense ? -1 : 1
        this.effectiveAmount = this.amount * factor
        return this.effectiveAmount
    }
}

@ChildEntity(TransactionType.AccountTransfer)
export class AccountTransfer extends Transaction {
    // przypisz właściwy typ po utworzeniu obiektu, nie czekaj na zapisanie w bazie
    constructor() {
        super()
        this.type = TransactionType.AccountTransfer
    }

    @ManyToOne(
        _type => Account,
        account => account.transferTransactions,
        { eager: true, onDelete: 'CASCADE' },
    )
    sourceAccount: Account

    @Column()
    sourceAccountId: number

    @ManyToOne(
        _type => Account,
        account => account.transferTransactions,
        { eager: true, onDelete: 'CASCADE' },
    )
    destinationAccount: Account

    @Column()
    destinationAccountId: number

    isOwnedByUser(user: User): boolean {
        if (!this.sourceAccount || !this.destinationAccount) {
            throw new Error(NoRelationData)
        }

        return (
            this.sourceAccount.userId === user.id &&
            this.destinationAccount.userId === user.id
        )
    }

    calculateEffectiveAmount(): number {
        // przelewy między kontami nie zmieniają ogólnej sumy
        this.effectiveAmount = 0
        return 0
    }
}
