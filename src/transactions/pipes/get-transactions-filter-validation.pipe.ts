import { PipeTransform, BadRequestException } from '@nestjs/common'
import { GetTransactionsFilterDto } from '../dto/get-transactions-filter.dto'

export class GetTransactionsFilterValidationPipe implements PipeTransform {
    transform(value: any): GetTransactionsFilterDto {
        const { year, month } = value
        const result: GetTransactionsFilterDto = {}

        if (month !== undefined) {
            const number = this.convertToInt(month)
            this.assertTrue(
                number > 0 && number < 13,
                'Nieprawidłowy numer miesiąca (dozwolone 1-12)',
            )
            result.month = number
        }

        if (year !== undefined) {
            const number = this.convertToInt(year)
            this.assertTrue(
                number >= 1900 && number < 3000,
                'Podany rok wykracza dozwolony zakres (1900-3000)',
            )

            // wymagaj miesiąca przy podaniu roku
            this.assertTrue(
                result.month !== undefined,
                'Podanie wyłącznie roku nie jest wspierane. Należy wskazać jeszcze miesiąc',
            )
            result.year = number
        }

        return result
    }

    private convertToInt(value: string): number {
        const number = parseInt(value, 10)

        if (isNaN(number)) {
            throw new BadRequestException(
                `Wartość "${value}" nie jest liczbą całkowitą`,
            )
        }

        return number
    }

    private assertTrue(testResult: boolean, message: string): void {
        if (!testResult) {
            throw new BadRequestException(message)
        }
    }
}
