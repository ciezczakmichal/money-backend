import { Calculator } from './calculator.class'
import { Category } from '../categories/category.entity'
import { CategoryType } from '../categories/category-type.enum'
import {
    Transaction,
    ExternalTransaction,
    AccountTransfer,
} from './transaction.entity'

function allPossibleCases<T>(arr: T[]): T[][] {
    if (arr.length === 1) {
        return [arr]
    } else {
        const result = []

        for (let i = 0; i < arr.length; i++) {
            const others = arr.filter(item => item !== arr[i])
            const allCasesOfRest = allPossibleCases(others)
            allCasesOfRest.forEach(single => result.push([arr[i], ...single]))
        }

        return result
    }
}

const createCategory = (id: number, type: CategoryType): Category => {
    const category = new Category()
    category.id = id
    category.type = type
    return category
}

const createExternalTransaction = (
    amount: number,
    date: string,
    accountId: number,
    category: Category,
): ExternalTransaction => {
    const trans = new ExternalTransaction()
    trans.amount = amount
    trans.date = date
    trans.accountId = accountId
    trans.categoryId = category.id
    trans.category = category

    return trans
}

const createAccountTransfer = (
    amount: number,
    date: string,
    sourceAccountId: number,
    destinationAccountId: number,
): AccountTransfer => {
    const trans = new AccountTransfer()
    trans.amount = amount
    trans.date = date
    trans.sourceAccountId = sourceAccountId
    trans.destinationAccountId = destinationAccountId

    return trans
}

class OtherTransaction extends Transaction {
    isOwnedByUser() {
        return false
    }
    calculateEffectiveAmount(): number {
        return 0
    }
}

describe('Calculator', () => {
    let calculator: Calculator

    beforeEach(() => {
        calculator = new Calculator()
    })

    it('dodanie niespieranej klasy transakcji rzuca wyjątek', () => {
        const add = () => calculator.addTransaction(new OtherTransaction())
        const addMany = () =>
            calculator.addTransactions([
                new OtherTransaction(),
                new OtherTransaction(),
            ])
        const addNull = () => calculator.addTransaction(null)

        expect(add).toThrow()
        expect(addMany).toThrow()
        expect(addNull).toThrow()
    })

    it('nieistniejące konto nie posiada transakcji', () => {
        expect(calculator.accountHasTransactions(0)).toEqual(false)
    })

    it('nieistniejąca kategoria nie występuje', () => {
        expect(calculator.categoryInTransactions(0)).toEqual(false)
    })

    it('konto posiada transakcje, a kategoria występuje - po dodaniu jednej transakcji', () => {
        const accountId = 1
        const category = createCategory(1, CategoryType.Expense)
        const transaction = createExternalTransaction(
            1.23,
            '2020-01-01',
            accountId,
            category,
        )

        calculator.addTransaction(transaction)
        expect(calculator.accountHasTransactions(accountId)).toEqual(true)
        expect(calculator.categoryInTransactions(category.id)).toEqual(true)
    })

    it('konto posiada transakcje, a kategoria występuje - po dodaniu kilku transakcji', () => {
        const categoryOne = createCategory(1, CategoryType.Expense)
        const categoryTwo = createCategory(2, CategoryType.Expense)

        calculator.addTransaction(
            createExternalTransaction(3.45, '2020-01-01', 1, categoryTwo),
        )
        calculator.addTransaction(
            createExternalTransaction(6.78, '2020-01-01', 2, categoryOne),
        )
        calculator.addTransaction(
            createExternalTransaction(100.2, '2020-01-01', 1, categoryOne),
        )

        expect(calculator.accountHasTransactions(1)).toEqual(true)
        expect(calculator.accountHasTransactions(2)).toEqual(true)

        expect(calculator.categoryInTransactions(categoryOne.id)).toEqual(true)
        expect(calculator.categoryInTransactions(categoryTwo.id)).toEqual(true)

        // nieistniejące
        expect(calculator.accountHasTransactions(3)).toEqual(false)
        expect(calculator.categoryInTransactions(3)).toEqual(false)
    })

    describe('testy metod pobierających wyliczenia - bez podania żadnych transakcji', () => {
        it('test pobierania danych zbiorczych dla wszystkich kont', () => {
            expect(calculator.queryTotalAccountsBalance()).toEqual(0)
            expect(
                calculator.queryTotalAccountsBalanceAtEndOfDay({
                    year: 2000,
                    month: 1,
                    day: 1,
                }),
            ).toEqual(0)
            expect(
                calculator.queryTotalAccountsBalanceAtEndOfDay({
                    year: 2100,
                    month: 12,
                    day: 31,
                }),
            ).toEqual(0)
        })

        it('test pobierania danych dla wskazanego konta', () => {
            expect(() => calculator.queryAccountBalance(1)).toThrow()
            expect(() =>
                calculator.queryAccountBalanceAtEndOfDay(1, {
                    year: 2100,
                    month: 12,
                    day: 31,
                }),
            ).toThrow()
        })

        it('test pobierania danych dla kategorii', () => {
            expect(() =>
                calculator.queryCategoryTotalAmountForDate(1, {
                    year: 2000,
                    month: 1,
                    day: 12,
                }),
            ).toThrow()

            expect(() =>
                calculator.queryCategoryTotalAmountForMonth(1, 2000, 1),
            ).toThrow()
        })
    })

    describe('testy metod pobierających wyliczenia - realne użycie', () => {
        const ticketsCategory = createCategory(1, CategoryType.Expense)
        const shoppingCategory = createCategory(2, CategoryType.Expense)
        const salaryCategory = createCategory(3, CategoryType.Proceeds)

        const testCategoryAmountForMonth = (
            category: Category,
            year: number,
            month: number,
            expected: number,
        ) => {
            expect(
                calculator.queryCategoryTotalAmountForMonth(
                    category.id,
                    year,
                    month,
                ),
            ).toEqual(expected)
        }

        // w JS dodawanie typu 0.1 + 0.2 daje ciekawe efekty
        it('niewielki test dokładności wyliczeń', () => {
            const accountId = 123
            const transactions: Transaction[] = [
                createExternalTransaction(
                    0.1,
                    '2020-05-10',
                    accountId,
                    salaryCategory,
                ),
                createExternalTransaction(
                    0.2,
                    '2020-05-10',
                    accountId,
                    salaryCategory,
                ),
            ]

            calculator.addTransactions(transactions)

            expect(calculator.accountHasTransactions(accountId)).toEqual(true)
            expect(
                calculator.categoryInTransactions(salaryCategory.id),
            ).toEqual(true)

            expect(calculator.queryAccountBalance(accountId)).toEqual(0.3)
            expect(calculator.queryTotalAccountsBalance()).toEqual(0.3)

            const date = {
                year: 2020,
                month: 5,
                day: 10,
            }

            expect(
                calculator.queryAccountBalanceAtEndOfDay(accountId, date),
            ).toEqual(0.3)
            expect(
                calculator.queryTotalAccountsBalanceAtEndOfDay(date),
            ).toEqual(0.3)

            expect(
                calculator.queryCategoryTotalAmountForDate(
                    salaryCategory.id,
                    date,
                ),
            ).toEqual(0.3)

            testCategoryAmountForMonth(salaryCategory, 2020, 5, 0.3)
        })

        describe('test wyliczeń dla transakcji dotyczących jednego konta', () => {
            const accountId = 123
            const transactions: Transaction[] = [
                createExternalTransaction(
                    1000,
                    '2020-05-31',
                    accountId,
                    salaryCategory,
                ),
                createExternalTransaction(
                    75.3,
                    '2020-06-01',
                    accountId,
                    ticketsCategory,
                ),
                createExternalTransaction(
                    121.53,
                    '2020-06-01',
                    accountId,
                    shoppingCategory,
                ),
                createExternalTransaction(
                    32,
                    '2020-07-05',
                    accountId,
                    shoppingCategory,
                ),
            ]

            it.each(allPossibleCases(transactions))('test', (...data) => {
                calculator.addTransactions(data)

                expect(calculator.accountHasTransactions(accountId)).toEqual(
                    true,
                )
                expect(
                    calculator.categoryInTransactions(ticketsCategory.id),
                ).toEqual(true)
                expect(
                    calculator.categoryInTransactions(shoppingCategory.id),
                ).toEqual(true)
                expect(
                    calculator.categoryInTransactions(salaryCategory.id),
                ).toEqual(true)

                expect(calculator.queryAccountBalance(accountId)).toEqual(
                    771.17,
                )
                expect(calculator.queryTotalAccountsBalance()).toEqual(771.17)

                const mayEndDate = {
                    year: 2020,
                    month: 5,
                    day: 31,
                }
                const julyDate = {
                    year: 2020,
                    month: 7,
                    day: 6,
                }

                expect(
                    calculator.queryAccountBalanceAtEndOfDay(
                        accountId,
                        mayEndDate,
                    ),
                ).toEqual(1000)
                expect(
                    calculator.queryAccountBalanceAtEndOfDay(
                        accountId,
                        julyDate,
                    ),
                ).toEqual(771.17)
                expect(
                    calculator.queryTotalAccountsBalanceAtEndOfDay(mayEndDate),
                ).toEqual(1000)
                expect(
                    calculator.queryTotalAccountsBalanceAtEndOfDay(julyDate),
                ).toEqual(771.17)

                // test dla dat wykraczających zakres transakcji
                expect(
                    calculator.queryTotalAccountsBalanceAtEndOfDay({
                        year: 2020,
                        month: 4,
                        day: 12,
                    }),
                ).toEqual(0)
                expect(
                    calculator.queryTotalAccountsBalanceAtEndOfDay({
                        year: 2020,
                        month: 8,
                        day: 1,
                    }),
                ).toEqual(771.17)

                // data z środka transkacji, ale w innym roku...
                expect(
                    calculator.queryTotalAccountsBalanceAtEndOfDay({
                        year: 2021,
                        month: 6,
                        day: 1,
                    }),
                ).toEqual(771.17)

                expect(
                    calculator.queryCategoryTotalAmountForDate(
                        salaryCategory.id,
                        mayEndDate,
                    ),
                ).toEqual(1000)
                expect(
                    calculator.queryCategoryTotalAmountForDate(
                        shoppingCategory.id,
                        mayEndDate,
                    ),
                ).toEqual(0)

                expect(
                    calculator.queryCategoryTotalAmountForDate(
                        salaryCategory.id,
                        julyDate,
                    ),
                ).toEqual(0)
                expect(
                    calculator.queryCategoryTotalAmountForDate(
                        ticketsCategory.id,
                        julyDate,
                    ),
                ).toEqual(0)

                testCategoryAmountForMonth(ticketsCategory, 2020, 5, 0)
                testCategoryAmountForMonth(shoppingCategory, 2020, 5, 0)
                testCategoryAmountForMonth(salaryCategory, 2020, 5, 1000)

                testCategoryAmountForMonth(ticketsCategory, 2020, 6, 75.3)
                testCategoryAmountForMonth(shoppingCategory, 2020, 6, 121.53)
                testCategoryAmountForMonth(salaryCategory, 2020, 6, 0)

                testCategoryAmountForMonth(ticketsCategory, 2020, 7, 0)
                testCategoryAmountForMonth(shoppingCategory, 2020, 7, 32)
                testCategoryAmountForMonth(salaryCategory, 2020, 7, 0)
            })
        })

        describe('test działania przelewów między kontami', () => {
            const accountOne = 1
            const accountTwo = 2
            const transactions: Transaction[] = [
                createExternalTransaction(
                    2500,
                    '2020-03-01',
                    accountOne,
                    salaryCategory,
                ),
                createAccountTransfer(
                    750,
                    '2020-03-01',
                    accountOne,
                    accountTwo,
                ),
                createExternalTransaction(
                    25,
                    '2020-03-02',
                    accountTwo,
                    ticketsCategory,
                ),
            ]

            it.each(allPossibleCases(transactions))('test', (...data) => {
                calculator.addTransactions(data)

                expect(calculator.accountHasTransactions(accountOne)).toEqual(
                    true,
                )
                expect(calculator.accountHasTransactions(accountTwo)).toEqual(
                    true,
                )

                expect(
                    calculator.categoryInTransactions(ticketsCategory.id),
                ).toEqual(true)
                expect(
                    calculator.categoryInTransactions(shoppingCategory.id),
                ).toEqual(false)
                expect(
                    calculator.categoryInTransactions(salaryCategory.id),
                ).toEqual(true)

                expect(calculator.queryAccountBalance(accountOne)).toEqual(1750)
                expect(calculator.queryAccountBalance(accountTwo)).toEqual(725)
                expect(calculator.queryTotalAccountsBalance()).toEqual(2475)

                const dayAfterPay = {
                    year: 2020,
                    month: 3,
                    day: 1,
                }
                const dayAfterExpenses = {
                    year: 2020,
                    month: 3,
                    day: 2,
                }

                expect(
                    calculator.queryAccountBalanceAtEndOfDay(
                        accountOne,
                        dayAfterPay,
                    ),
                ).toEqual(1750)
                expect(
                    calculator.queryAccountBalanceAtEndOfDay(
                        accountOne,
                        dayAfterExpenses,
                    ),
                ).toEqual(1750)
                expect(
                    calculator.queryAccountBalanceAtEndOfDay(
                        accountTwo,
                        dayAfterPay,
                    ),
                ).toEqual(750)
                expect(
                    calculator.queryAccountBalanceAtEndOfDay(
                        accountTwo,
                        dayAfterExpenses,
                    ),
                ).toEqual(725)

                expect(
                    calculator.queryTotalAccountsBalanceAtEndOfDay(dayAfterPay),
                ).toEqual(2500)
                expect(
                    calculator.queryTotalAccountsBalanceAtEndOfDay(
                        dayAfterExpenses,
                    ),
                ).toEqual(2475)

                testCategoryAmountForMonth(ticketsCategory, 2020, 3, 25)
                // testCategoryAmountForMonth(shoppingCategory, 2020, 3, 0)
                testCategoryAmountForMonth(salaryCategory, 2020, 3, 2500)
            })
        })

        describe('test poprawności operacji na przełomie roku', () => {
            const accountOne = 1
            const accountTwo = 2
            const transactions: Transaction[] = [
                createAccountTransfer(
                    100,
                    '2019-12-27',
                    accountOne,
                    accountTwo,
                ),
                createExternalTransaction(
                    80,
                    '2019-12-31',
                    accountTwo,
                    shoppingCategory,
                ),
                createExternalTransaction(
                    9.99,
                    '2020-01-01',
                    accountTwo,
                    shoppingCategory,
                ),
                createAccountTransfer(20, '2020-01-01', accountOne, accountTwo),
            ]

            it.each(allPossibleCases(transactions))('test', (...data) => {
                calculator.addTransactions(data)

                // bez testów czy konto jest, czy kategoria jest...

                expect(calculator.queryAccountBalance(accountOne)).toEqual(-120)
                expect(calculator.queryAccountBalance(accountTwo)).toEqual(
                    30.01,
                )
                expect(calculator.queryTotalAccountsBalance()).toEqual(-89.99)

                const dayPrevYear = {
                    year: 2019,
                    month: 12,
                    day: 31,
                }
                const dayNewYear = {
                    year: 2020,
                    month: 1,
                    day: 1,
                }

                expect(
                    calculator.queryAccountBalanceAtEndOfDay(
                        accountOne,
                        dayPrevYear,
                    ),
                ).toEqual(-100)
                expect(
                    calculator.queryAccountBalanceAtEndOfDay(
                        accountOne,
                        dayNewYear,
                    ),
                ).toEqual(-120)
                expect(
                    calculator.queryAccountBalanceAtEndOfDay(
                        accountTwo,
                        dayPrevYear,
                    ),
                ).toEqual(20)
                expect(
                    calculator.queryAccountBalanceAtEndOfDay(
                        accountTwo,
                        dayNewYear,
                    ),
                ).toEqual(30.01)

                expect(
                    calculator.queryTotalAccountsBalanceAtEndOfDay(dayPrevYear),
                ).toEqual(-80)
                expect(
                    calculator.queryTotalAccountsBalanceAtEndOfDay(dayNewYear),
                ).toEqual(-89.99)

                testCategoryAmountForMonth(shoppingCategory, 2019, 12, 80)
                testCategoryAmountForMonth(shoppingCategory, 2020, 1, 9.99)

                // dodatkowe testy wykraczające poza lata transakcji
                expect(
                    calculator.queryTotalAccountsBalanceAtEndOfDay({
                        year: 2015,
                        month: 12,
                        day: 31,
                    }),
                ).toEqual(0)
                expect(
                    calculator.queryTotalAccountsBalanceAtEndOfDay({
                        year: 2016,
                        month: 1,
                        day: 1,
                    }),
                ).toEqual(0)
                expect(
                    calculator.queryTotalAccountsBalanceAtEndOfDay({
                        year: 2035,
                        month: 1,
                        day: 1,
                    }),
                ).toEqual(-89.99)
                expect(
                    calculator.queryTotalAccountsBalanceAtEndOfDay({
                        year: 2035,
                        month: 12,
                        day: 31,
                    }),
                ).toEqual(-89.99)
            })
        })
    })
})
