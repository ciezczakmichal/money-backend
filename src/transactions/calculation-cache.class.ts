import { Injectable } from '@nestjs/common'
import { Calculator } from './calculator.class'
import { User } from '../auth/user.entity'
import { Transaction } from './transaction.entity'

declare type LoaderFunction = (user: User) => Promise<Transaction[]>

@Injectable()
export class CalculationCache {
    // powiązanie ID użytkownika z wyliczeniami
    private data: Map<number, Calculator>

    // powiązanie ID użytkownika z obietnicą z metody reloadDataImpl
    private loading: Map<number, Promise<void>>

    private loaderFunction: LoaderFunction

    constructor() {
        this.data = new Map<number, Calculator>()
        this.loading = new Map<number, Promise<void>>()
        this.loaderFunction = null
    }

    async loadDataIfNecessary(user: User): Promise<void> {
        const { id } = user

        if (!this.data.has(id)) {
            const loadingPromise = this.loading.get(id)

            if (loadingPromise) {
                await loadingPromise
            } else {
                await this.reloadData(user)
            }
        }
    }

    async reloadData(user: User): Promise<void> {
        if (!this.loaderFunction) {
            throw new Error('Nie przypisano funkcji ładującej')
        }

        const { id } = user
        this.data.delete(id)

        const promise = this.reloadDataImpl(user)
        this.loading.set(id, promise)
        await promise
        this.loading.delete(id)
    }

    bindLoaderFunction(callback: LoaderFunction): void {
        this.loaderFunction = callback
    }

    getCalculator(user: User): Calculator {
        const result = this.data.get(user.id)

        if (!result) {
            throw new Error(
                `Nie odnaleziono danych dla użytkownika o ID ${user.id}`,
            )
        }

        return result
    }

    private async reloadDataImpl(user: User): Promise<void> {
        const transactions = await this.loaderFunction(user)
        const calculator = new Calculator()
        calculator.addTransactions(transactions)
        this.data.set(user.id, calculator)
    }
}
