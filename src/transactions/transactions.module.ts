import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { TransactionsController } from './transactions.controller'
import { TransactionsService } from './transactions.service'
import { TransactionRepository } from './transaction.repository'
import { AuthModule } from '../auth/auth.module'
import { AccountsModule } from '../accounts/accounts.module'
import { CategoriesModule } from '../categories/categories.module'
import { CalculationCache } from './calculation-cache.class'

@Module({
    imports: [
        TypeOrmModule.forFeature([TransactionRepository]),
        AuthModule,
        AccountsModule,
        CategoriesModule,
    ],
    controllers: [TransactionsController],
    providers: [TransactionsService, CalculationCache],
})
export class TransactionsModule {}
