import {
    Injectable,
    NotFoundException,
    InternalServerErrorException,
    BadRequestException,
} from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { AccountsService } from '../accounts/accounts.service'
import {
    CategoriesService,
    IncludeAmountCalculation,
    IncludeInternalCategories,
} from '../categories/categories.service'
import { TransactionRepository } from './transaction.repository'
import {
    Transaction,
    ExternalTransaction,
    AccountTransfer,
} from './transaction.entity'
import { User } from '../auth/user.entity'
import { CreateTransactionDto } from './dto/create-transaction.dto'
import { TransactionType } from './transaction-type.enum'
import { UpdateTransactionDto } from './dto/update-transaction.dto'
import { TransactionForeignKeysCheckerData } from './transaction-checker-data.interface'
import { GetTransactionsFilterDto } from './dto/get-transactions-filter.dto'
import { Category } from '../categories/category.entity'
import { Account } from '../accounts/account.entity'
import { CalculationCache } from './calculation-cache.class'
import {
    TransactionListDto,
    PeriodName,
    TransactionsPerDay,
} from './dto/transaction-list.dto'
import {
    decodeDateComponents,
    encodeDateComponents,
    getDaysInMonth,
    DateComponents,
    today,
} from './helpers/date-utils.helper'

const mergeProps = (object: any, props: string[], dataSource: any) => {
    props.forEach(prop => {
        if (dataSource[prop] !== undefined) {
            object[prop] = dataSource[prop]
        }
    })
}

@Injectable()
export class TransactionsService {
    constructor(
        @InjectRepository(TransactionRepository)
        private transactionRepository: TransactionRepository,
        private accountsService: AccountsService,
        private categoriesService: CategoriesService,
        private calculations: CalculationCache,
    ) {
        calculations.bindLoaderFunction(
            async (user: User): Promise<Transaction[]> => {
                const [categories, transactions] = await Promise.all([
                    // wyłącz wyliczenia, inaczej mamy pętlę wywołań
                    categoriesService.getCategories(
                        IncludeInternalCategories.Include,
                        user,
                        IncludeAmountCalculation.Exclude,
                    ),
                    transactionRepository.getTransactions(user),
                ])

                // kalkulator wymaga danych kategorii dla transakcji
                this.bindRelationsDataToTransactions(
                    transactions,
                    [], // konta nie są potrzebne
                    categories,
                )

                return transactions
            },
        )
    }

    async getTransactions(
        user: User,
        filterDto: GetTransactionsFilterDto,
    ): Promise<TransactionListDto> {
        let { year, month } = filterDto
        const now = today()

        if (!year) {
            year = now.year
        }

        if (!month) {
            month = now.month
        }

        const lastDay = getDaysInMonth(month, year)

        const fromDate: DateComponents = {
            year,
            month,
            day: 1,
        }
        const toDate: DateComponents = {
            year,
            month,
            day: lastDay,
        }

        const fromDateString = encodeDateComponents(fromDate)
        const toDateString = encodeDateComponents(toDate)

        const [transactions, accounts, categories] = await Promise.all([
            this.transactionRepository.getTransactions(user, {
                fromDate: fromDateString,
                toDate: toDateString,
            }),
            this.accountsService.getAccounts(user),
            this.categoriesService.getCategories(
                IncludeInternalCategories.Include,
                user,
            ),
            this.calculations.loadDataIfNecessary(user),
        ])

        // dołącz dane obiektów do poszczególnych transakcji
        this.bindRelationsDataToTransactions(transactions, accounts, categories)

        const calculator = this.calculations.getCalculator(user)
        const startBalance = calculator.queryTotalAccountsBalanceAtEndOfDay({
            year,
            month: month - 1,
            day: 31,
        }) // wskaż ostatni możliwy dzień w poprzednim miesiącu
        const endBalance = calculator.queryTotalAccountsBalanceAtEndOfDay(
            toDate,
        )

        return {
            period: {
                name: PeriodName.Month,
                from: fromDateString,
                to: toDateString,
            },
            startBalance,
            endBalance,
            data: this.groupByDay(transactions),
            accounts,
            categories: categories.filter(category => !category.internal),
        }
    }

    async getTransactionsForCategory(
        categoryId: number,
        user: User,
    ): Promise<TransactionsPerDay[]> {
        const { firstDate, lastDate } = this.getCurrentMonthRange()

        const [transactions, accounts, categories] = await Promise.all([
            this.transactionRepository.getTransactions(user, {
                fromDate: encodeDateComponents(firstDate),
                toDate: encodeDateComponents(lastDate),
            }),
            this.accountsService.getAccounts(user),
            this.categoriesService.getCategories(
                IncludeInternalCategories.Include,
                user,
            ),
        ])

        // dołącz dane obiektów do poszczególnych transakcji
        this.bindRelationsDataToTransactions(transactions, accounts, categories)

        return this.groupByDay(
            transactions.filter(
                trans =>
                    trans instanceof ExternalTransaction &&
                    trans.categoryId === categoryId,
            ),
        )
    }

    async getAllTransactions(user: User): Promise<Transaction[]> {
        // transakcje wymagają danych kategorii do wyliczenia efektywnej kwoty
        const [categories, accounts, transactions] = await Promise.all([
            this.categoriesService.getCategories(
                IncludeInternalCategories.Include,
                user,
            ),
            this.accountsService.getAccounts(user),
            this.transactionRepository.getTransactions(user),
        ])

        this.bindRelationsDataToTransactions(transactions, accounts, categories)

        transactions.forEach(trans => trans.calculateEffectiveAmount())

        return transactions
    }

    async getTransactionById(id: number, user: User): Promise<Transaction> {
        const found = await this.transactionRepository.getTransactionById(
            id,
            user,
        )

        if (!found) {
            throw new NotFoundException(`Transakcja o ID "${id}" nie istnieje`)
        }

        // dodaj właściwość
        found.calculateEffectiveAmount()

        // zwracana transakcja ma powiązania z innymi obiektami

        return found
    }

    async createTransaction(
        dto: CreateTransactionDto,
        user: User,
        option: IncludeInternalCategories = IncludeInternalCategories.Exclude,
    ): Promise<Transaction> {
        const checkerData = TransactionForeignKeysCheckerData.fromCreateTransactionDto(
            dto,
        )
        await this.checkTransactionForeignKeys(checkerData, user, option)

        if (dto.type === TransactionType.AccountTransfer) {
            this.validateAccountsNotTheSame(
                dto.sourceAccountId,
                dto.destinationAccountId,
            )
        }

        const result = await this.transactionRepository.createTransaction(dto)
        this.recalculateTransactions(user)
        return result
    }

    async updateTransaction(
        id: number,
        updateTransactionDto: UpdateTransactionDto,
        user: User,
    ): Promise<Transaction> {
        const transaction = await this.getTransactionById(id, user)

        const updateableProps = ['amount', 'date', 'comment']
        mergeProps(transaction, updateableProps, updateTransactionDto)

        // informuj o braku możliwości zmiany typu transakcji
        if (
            updateTransactionDto.type &&
            updateTransactionDto.type !== transaction.type
        ) {
            throw new BadRequestException(
                'Zmiana typu istniejącej transakcji nie jest wspierana',
            )
        }

        if (transaction instanceof ExternalTransaction) {
            mergeProps(
                transaction,
                ['accountId', 'categoryId'],
                updateTransactionDto,
            )
        } else if (transaction instanceof AccountTransfer) {
            mergeProps(
                transaction,
                ['sourceAccountId', 'destinationAccountId'],
                updateTransactionDto,
            )

            this.validateAccountsNotTheSame(
                transaction.sourceAccountId,
                transaction.destinationAccountId,
            )
        }

        const checkerData = TransactionForeignKeysCheckerData.fromTransaction(
            transaction,
        )

        await this.checkTransactionForeignKeys(
            checkerData,
            user,
            IncludeInternalCategories.Exclude,
        )

        // przelicz na nowo właściwość
        transaction.calculateEffectiveAmount()

        /* Usuwaj z dwóch powodów:
         * 1) zmiana ID nie powodowała zmiany danych obiektu (np. accountId - account)
         * 2) usunięcie właściwości z obiektem powoduje, że zapisywanie zmian w ID działa */
        this.deleteEntitiesProperties(transaction)

        await transaction.save()
        this.recalculateTransactions(user)
        return transaction
    }

    async deleteTransaction(id: number, user: User): Promise<void> {
        const deleted = await this.transactionRepository.deleteTransaction(
            id,
            user,
        )

        if (!deleted) {
            throw new NotFoundException(`Transakcja o ID "${id}" nie istnieje`)
        }

        this.recalculateTransactions(user)
    }

    recalculateTransactions(user: User): Promise<void> {
        return this.calculations.reloadData(user)
    }

    async getAccountBalance(accountId: number, user: User): Promise<number> {
        await this.calculations.loadDataIfNecessary(user)
        const calculator = this.calculations.getCalculator(user)

        return calculator.accountHasTransactions(accountId)
            ? calculator.queryAccountBalance(accountId)
            : 0.0
    }

    async getCategoryTotalAmountForCurrentMonth(
        categoryId: number,
        user: User,
    ): Promise<number> {
        await this.calculations.loadDataIfNecessary(user)
        const calculator = this.calculations.getCalculator(user)

        const { year, month } = today()

        return calculator.categoryInTransactions(categoryId)
            ? calculator.queryCategoryTotalAmountForMonth(
                  categoryId,
                  year,
                  month,
              )
            : 0.0
    }

    async getCategoryTotalAmountForDate(
        categoryId: number,
        date: DateComponents,
        user: User,
    ): Promise<number> {
        await this.calculations.loadDataIfNecessary(user)
        const calculator = this.calculations.getCalculator(user)

        return calculator.categoryInTransactions(categoryId)
            ? calculator.queryCategoryTotalAmountForDate(categoryId, date)
            : 0.0
    }

    private async checkTransactionForeignKeys(
        data: TransactionForeignKeysCheckerData,
        user: User,
        option: IncludeInternalCategories,
    ): Promise<void> {
        /* Funkcje zwracające obiekty biorą pod uwagę dostępność danych
         * dla konkretnego użytkownika, stąd sprawdzane są również i
         * uprawnienia. */
        try {
            switch (data.type) {
                case TransactionType.ExternalTransaction:
                    const account = this.accountsService.getAccountById(
                        data.accountId,
                        user,
                    )
                    const category = this.categoriesService.getCategoryById(
                        data.categoryId,
                        option,
                        user,
                    )
                    await Promise.all([account, category])
                    break

                case TransactionType.AccountTransfer:
                    const sourceAccount = this.accountsService.getAccountById(
                        data.sourceAccountId,
                        user,
                    )
                    const destinationAccount = this.accountsService.getAccountById(
                        data.destinationAccountId,
                        user,
                    )
                    await Promise.all([sourceAccount, destinationAccount])
                    break

                default:
                    throw new Error(
                        `Nierozpoznany typ transakcji "${data.type}"`,
                    )
            }
        } catch (error) {
            if (error instanceof NotFoundException) {
                throw new NotFoundException(
                    'Powiązania z innymi obiektami są nieprawidłowe. ' +
                        error.message,
                )
            } else {
                throw new InternalServerErrorException()
            }
        }
    }

    private deleteEntitiesProperties(transaction: Transaction) {
        if (transaction instanceof ExternalTransaction) {
            delete transaction.account
            delete transaction.category
        } else if (transaction instanceof AccountTransfer) {
            delete transaction.sourceAccount
            delete transaction.destinationAccount
        }
    }

    private bindRelationsDataToTransactions(
        transactions: Transaction[],
        accounts: Account[],
        categories: Category[],
    ): void {
        const findAccount = (id: number) =>
            accounts.find(account => account.id === id) || null
        const findCategory = (id: number) =>
            categories.find(category => category.id === id) || null

        transactions.forEach(item => {
            if (item instanceof ExternalTransaction) {
                item.account = findAccount(item.accountId)
                item.category = findCategory(item.categoryId)
            } else if (item instanceof AccountTransfer) {
                item.sourceAccount = findAccount(item.sourceAccountId)
                item.destinationAccount = findAccount(item.destinationAccountId)
            }
        })
    }

    private groupByDay(input: Transaction[]): TransactionsPerDay[] {
        const result: TransactionsPerDay[] = []

        input.forEach(transaction => {
            // dodaj właściwość w odpowiedzi
            const effectiveAmount = transaction.calculateEffectiveAmount()

            const dayData = result.find(day => day.date === transaction.date)

            if (dayData) {
                dayData.transactions.push(transaction)
                dayData.totalEffectiveAmount += effectiveAmount
            } else {
                const { year, month, day } = decodeDateComponents(
                    transaction.date,
                )

                result.push({
                    date: transaction.date,
                    year,
                    month,
                    day,
                    totalEffectiveAmount: effectiveAmount,
                    transactions: [transaction],
                })
            }
        })

        return result
    }

    private validateAccountsNotTheSame(
        sourceAccountId: number,
        destinationAccountId: number,
    ): void {
        if (sourceAccountId === destinationAccountId) {
            throw new BadRequestException(
                'Konto docelowe nie może być jednocześnie kontem źródłowym',
            )
        }
    }

    private getCurrentMonthRange(): {
        firstDate: DateComponents
        lastDate: DateComponents
    } {
        const { year, month } = today()
        const lastDay = getDaysInMonth(month, year)

        const firstDate: DateComponents = {
            year,
            month,
            day: 1,
        }
        const lastDate: DateComponents = {
            year,
            month,
            day: lastDay,
        }

        return {
            firstDate,
            lastDate,
        }
    }
}
