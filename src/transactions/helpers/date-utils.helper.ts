import * as dayjs from 'dayjs'
import * as customParseFormat from 'dayjs/plugin/customParseFormat'
import { DateFormatString } from './date-constants.helper'

dayjs.extend(customParseFormat)

export interface DateComponents {
    year: number
    month: number
    day: number
}

export const encodeDateComponents = (
    dateComponents: DateComponents,
): string => {
    const { year, month, day } = dateComponents
    const date = dayjs()
        .year(year)
        .month(month - 1) // JS...
        .date(day)

    if (!date.isValid()) {
        throw new Error('Przekazane dane nie kodują poprawnej daty')
    }

    return date.format(DateFormatString)
}

export const decodeDateComponents = (dateText: string): DateComponents => {
    const date = dayjs(dateText, DateFormatString)

    if (!date.isValid()) {
        throw new Error(`Ciąg "${dateText}" nie jest prawidłową datą`)
    }

    return {
        year: date.year(),
        month: date.month() + 1, // JS...
        day: date.date(),
    }
}

export const today = (): DateComponents => {
    const now = new Date()

    return {
        year: now.getFullYear(),
        month: now.getMonth() + 1, // JS...
        day: now.getDate(),
    }
}

/**
 * Zwraca true, jeśli data B jest późniejsza niż data A.
 * @param dateA
 * @param dateB
 */
export const isDateAfter = (
    dateA: DateComponents,
    dateB: DateComponents,
): boolean => {
    if (dateB.year > dateA.year) {
        return true
    } else if (dateB.year === dateA.year) {
        if (dateB.month > dateA.month) {
            return true
        } else if (dateB.month === dateA.month) {
            return dateB.day > dateA.day
        }
    }

    return false
}

// https://stackoverflow.com/a/27947860
export const getDaysInMonth = (m: number, y: number): number => {
    return m === 2
        ? !(y % 4 || (!(y % 100) && y % 400))
            ? 29
            : 28
        : 30 + ((m + (m >> 3)) & 1)
}

export const getDaysInCurrentMonth = (): number => {
    const { month, year } = today()
    return getDaysInMonth(month, year)
}
