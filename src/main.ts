import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import { ConfigService } from '@nestjs/config'
import { HttpExceptionFilter } from './filters/http-exception.filter'

async function bootstrap() {
    const app = await NestFactory.create(AppModule)

    const configService = app.get(ConfigService)
    const enableCors = configService.get<boolean>('server.enableCors')
    const port = configService.get<number>('server.port')

    if (enableCors) {
        app.enableCors()
    }

    app.useGlobalFilters(new HttpExceptionFilter())

    await app.listen(port)
}
bootstrap()
