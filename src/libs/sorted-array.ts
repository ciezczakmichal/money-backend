import * as SortedArrayLib from 'sorted-array'

export class SortedArray<T> {
    private wrappedClass: SortedArrayLib

    constructor(
        initArray: T[] = [],
        comparePropOrFunction?: string | ((a: T, b: T) => number),
    ) {
        if (comparePropOrFunction) {
            if (typeof comparePropOrFunction === 'string') {
                // tworzy wersję porównującą po podanej właściwości
                this.wrappedClass = SortedArrayLib.comparing(
                    comparePropOrFunction,
                    initArray,
                )
            } else {
                // wersja z użyciem własnej funkcji porównującej
                this.wrappedClass = new SortedArrayLib(
                    initArray,
                    comparePropOrFunction,
                )
            }
        } else {
            this.wrappedClass = new SortedArrayLib(initArray)
        }
    }

    insert(item: T): void {
        this.wrappedClass.insert(item)
    }

    /**
     * Zwraca indeks do elementu.
     * @param item
     */
    search(item: T): number {
        return this.wrappedClass.search(item)
    }

    // jest jeszcze remove

    /* Własne */

    getArray(): T[] {
        return this.wrappedClass.array
    }

    getLength(): number {
        return this.wrappedClass.length
    }

    find(predicate: (item: T) => boolean) {
        return this.getArray().find(predicate)
    }
}
