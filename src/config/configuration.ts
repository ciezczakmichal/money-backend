export default () => ({
    server: {
        port: parseInt(process.env.PORT, 10) || 3000,
        enableCors: process.env.ENABLE_CORS === 'true',
    },
    database: {
        type: 'postgres',
        host: process.env.DATABASE_HOST || 'localhost',
        port: parseInt(process.env.DATABASE_PORT, 10) || 5432,
        username: process.env.DATABASE_USERNAME,
        password: process.env.DATABASE_PASSWORD,
        database: process.env.DATABASE_DATABASE,
        schema: process.env.DATABASE_SCHEMA,
        ssl: process.env.DATABASE_SSL === 'true',

        entities: [__dirname + '/../**/*.entity.{js,ts}'],
        synchronize: process.env.DATABASE_SYNCHRONIZE || true,
    },
    jwt: {
        secret: process.env.JWT_SECRET,
    },
})
