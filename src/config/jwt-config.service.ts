import { JwtOptionsFactory, JwtModuleOptions } from '@nestjs/jwt'
import { Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'

@Injectable()
export class JwtConfigService implements JwtOptionsFactory {
    constructor(private configService: ConfigService) {}

    createJwtOptions(): JwtModuleOptions {
        return {
            secret: this.configService.get<string>('jwt.secret'),
            signOptions: {
                expiresIn: 3600 * 24, // 24h ważności
            },
        }
    }
}
