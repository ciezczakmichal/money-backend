import { TypeOrmOptionsFactory, TypeOrmModuleOptions } from '@nestjs/typeorm'
import { Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'

@Injectable()
export class TypeOrmConfigService implements TypeOrmOptionsFactory {
    constructor(private configService: ConfigService) {}

    createTypeOrmOptions(): TypeOrmModuleOptions {
        const service = this.configService

        const useSsl = service.get<boolean>('database.ssl')
        const sslOptions = useSsl
            ? {
                  ssl: {
                      // pozwól na niezaufane, własne certyfikaty
                      rejectUnauthorized: false,
                  },
              }
            : null

        return {
            type: service.get<any>('database.type'),
            host: service.get<string>('database.host'),
            port: service.get<number>('database.port'),
            username: service.get<string>('database.username'),
            password: service.get<string>('database.password'),
            database: service.get<string>('database.database'),
            schema: service.get<string>('database.schema'),
            ...sslOptions,

            entities: [__dirname + '/../**/*.entity.{js,ts}'],
            synchronize: service.get<boolean>('database.synchronize'),
        }
    }
}
